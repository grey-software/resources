import theme from "grey-docs";

export default theme({
  components: true,
  content: {
    liveEdit: false,
  },
  docs: {
    primaryColor: "#64748b",
  },
  head: {
    title: "Resources by Grey Software | Democratizing Software Education!",
    meta: [
      {
        name: "og:title",
        content:
          "Resources by Grey Software | Democratizing Software Education!",
      },
      {
        name: "og:description",
        content:
          "A website where we curate useful resources for the open-source software ecosystem!",
      },
      {
        name: "og:image",
        content: "/preview.png",
      },
      {
        name: "twitter:card",
        content: "summary_large_image",
      },
      {
        name: "twitter:title",
        content:
          "Resources by Grey Software | Democratizing Software Education!",
      },
      {
        name: "twitter:description",
        content:
          "A website where we curate useful resources for the open-source software ecosystem!",
      },
      {
        name: "twitter:image",
        content: "/preview.png",
      },
    ],
    script: [
      {
        src: "https://plausible.io/js/plausible.js",
        async: true,
        defer: true,
        "data-domain": "resources.grey.software",
      },
    ],
  },
});
