---
title: Contributing Guide
description: Learn about how to add resources to our website.
category: Contributing
position: 102
---

## Detailed Guide

This guide will help you learn how to add formatted resources to our website!

### Visit the resources repo

<cta-button text="Visit Repo" link="https://gitlab.com/grey-software/resources"></cta-button>

### Fork the repo

<alert type="info"> This step is for those contributors who do not have team member access to the repository.</alert>

A fork is a duplicate of the original repository where you can make changes without affecting the source project.

![](https://i.imgur.com/WEY2YUe.png)

Click on the fork button.

![](https://i.imgur.com/LndPMaB.png)

Select your namespace and hit **Fork Project**

### Create a new branch

![](https://i.imgur.com/gWK8H5V.png)

![](https://i.imgur.com/gania9p.png)

The name of the branch should be in lower-kabab-case. Try to keep it precise and to the point.

We've named our branch example, but you could try `ai-resources` or `updating-queue`.

After naming your branch, click **Create branch**

### Start Editing

![](https://i.imgur.com/lpfrb4P.png)

To get started with editing your copy of the resources repo, you can either clone it locally or open it in Gitpod.

#### Gitpod

Before launching Gitpod, make sure you are in the same branch as you have created earlier.

<alert type="info">We will be using Gitpod for the rest of this tutorial, but the steps for developing on Gitpod or your local copy of the repo should be the same.
Gitpod provides you with a VS Code-powered IDE for any Gitlab or Github repo.</alert>

#### Local Development

```shell=
git clone https://gitlab.com/grey-software/resources.git
cd resources
```

#### Web IDE

If you're making a simple addition to the queue, you don't need to run a live preview to ensure your changes are correctly formatted.

Simply open the queue file in the Web IDE, and you'll find it much easier to submit your edits without leaving Gitlab.


### Install Project Dependencies

![](https://i.imgur.com/wWVFosn.png)

After launching Gitpod, type `yarn` to install the project's dependencies.

```shell=
yarn
```

### Start Developing

![](https://i.imgur.com/frwu4gd.png)

Then type `yarn dev` to start a local server that will allow you to see your development changes.

```shell=
yarn dev
```

<alert type="info">
Answer `y` or `n` to whether or not you'd like to contribute your data to improve the NuxtJS developer experience. We usually answer `y`
</alert>

### Open a preview browser

![](https://i.imgur.com/0UmWEYQ.png)

Select **Open Browser** to preview your changes to the website in another tab.

<alert type="info">Its OK to allow popups from gitpod if your browser prompts you</alert>

![](https://i.imgur.com/cem0Hnj.png)

### Edit a Resource Page(s)

![](https://i.imgur.com/pL0NwzS.png)

On the left side of the Gitpod IDE, you'll find the project file explorer, through which you can navigate to the `content` folder, where you'll find the Markdown files that contain the content for our resources website's pages.

Each category has several pages.
We will leave it up to you to explore them and decide where you want to share your knowledge.

### You have few options here

<details>
<summary>The page you want to add to already exists.</summary>

Please proceed to the next step [Add YAML Metadata](/contributing/guide#add-yaml-metadata).

</details>

<details>
<summary>You need to create a new page</summary>

![](https://i.imgur.com/fCzB7jS.png)

1. Right-click on your category's folder, and hit `New File`.
2. Name the file in lower-case with no spaces, only hyphens, and with the Markdown _.md_ extension .e.g **computer-science.md**
</details>

<details>
<summary>You need to create a new category. </summary>

![](https://i.imgur.com/fCzB7jS.png)

1. Right-click on **en** and select the `New Folder` option.
2. Name the folder in lower-case with hyphens instead of spaces.
3. To create a page in the same category, follow these two steps.
   - Right-click on your category's folder, and hit `New File`.
   - Name the file in lower-case with no spaces, only hyphens, and with the Markdown _.md_ extension .e.g **computer-science.md**

</details>

### Add YAML Metadata

![](https://i.imgur.com/BoJRHud.png)

If you're adding resources to a page that already exists, then you don't have to add the `title`, `description`, and `position` metadata to the YAML frontmatter in the page.

However, for a _New Page_, you need to add the following metadata.

```yaml
- title: _name of the page_
- description: We're compiling a library of useful **\_\_\_** from around the Internet to help you on your software journey!
- position: _position of the page_
- category: _name of the category_
```

### Format with Markdown & components

- **For video link**, we use  [`YoutubeVideo.vue`](https://gitlab.com/grey-software/templates/grey-docs/-/blob/master/packages/grey-docs/src/components/global/YoutubeVideo.vue) component to embed Youtube videos so that they remain responsive!

<code-group>
<code-block label="Markdown" active>

```
<youtube-video id="Ag1AKIl_2GM"> </youtube-video>

```
  </code-block>
<code-block label="Output">
<br></br>

<youtube-video id="Ag1AKIl_2GM"> </youtube-video>

 </code-block>
</code-group>

- We use the [`CtaButton.vue`](https://gitlab.com/grey-software/templates/grey-docs/-/blob/master/packages/grey-docs/src/components/global/CtaButton.vue) component to draw users to important links.


<code-group>
<code-block label="Markdown" active>

```
<cta-button text="CTA Button" link="https://grey.software"></cta-button>

```
  </code-block>
<code-block label="Output">
<br></br>

<cta-button text="CTA Button" link="https://grey.software"></cta-button>

 </code-block>
</code-group>

### Submit your changes

![](https://i.imgur.com/XBgMJ3R.png)

After writing all the content, click on the version control button. It will show you the number of changes you have made and allow you to submit your changes.

#### Stage your changes

![](https://i.imgur.com/gUNI3B8.png)

1. Click on **+** to stage all changes.
2. Click on the checkmark to commit.

#### Add a commit message

![](https://i.imgur.com/mljyCD9.png)

The commit message should describe what changes your commit makes to the code's behavior, and it should be short and to the point. 

e.g., If you have added a new page to the website, the commit message should say **"update: added NAME_OF_THE_PAGE"**


#### Push your changes

![](https://i.imgur.com/K23Xy21.png)

Click on the three dots to open the dropdown menu and click push.

<alert type="info">You have successfully commit and push your changes.</alert>

#### Create a Merge Request

![](https://i.imgur.com/ziI5gwG.png)

Now create a Merge Request.

![](https://i.imgur.com/Ru5HlSp.png)

Select your source branch from the dropdown menu and make sure the target branch is **grey-software/resources**

![](https://i.imgur.com/CuLnKXF.png)

![](https://i.imgur.com/vBOm66A.png)

1. Fill in the title and the description sections.
2. Add the `Stage :: Inspection` Label
3. Click the **Create merge request** button

<alert type="success">Congratulations <br/> We will review your work and get back to you!</alert>
