---
title: Cheat Sheets
description: We're compiling a library of useful application development cheat sheets from around the internet to help you on your software journey!
position: 14002
category: Application Development
---

## Java

### Java Cheatsheet

[![Java Preview](https://i.imgur.com/N0NaEVV.png)](https://programmingwithmosh.com/wp-content/uploads/2019/07/Java-Cheat-Sheet.pdf)

[Source : Mosh Hamedani](https://codewithmosh.com/)

<cta-button text="View" link="https://programmingwithmosh.com/wp-content/uploads/2019/07/Java-Cheat-Sheet.pdf"></cta-button>


### Java Cheatsheet 

[![Java Cheatsheet Preview](https://i.imgur.com/66ENot0.png)](https://www.codewithharry.com/blogpost/java-cheatsheet)

[Source : CodeWithHarry.com](https://www.codewithharry.com/)

<cta-button text="View" link="https://www.codewithharry.com/blogpost/java-cheatsheet"></cta-button>

</br>

<cta-button text="More Cheatsheets On Java" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#java"></cta-button>

## Kotlin

### Kotlin Cheatsheet and Quick Reference 

[![TensorFlow Quick Reference Table Preview](https://i.imgur.com/Xo2Jggk.png)](https://koenig-media.raywenderlich.com/uploads/2019/11/RW-Kotlin-Cheatsheet-1.1.pdf)

[Source : raywenderlich.com](https://www.raywenderlich.com/)

<cta-button text="View" link="https://koenig-media.raywenderlich.com/uploads/2019/11/RW-Kotlin-Cheatsheet-1.1.pdf"></cta-button>

</br>

<cta-button text="More Cheatsheets On Kotlin" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#kotlin"></cta-button>
