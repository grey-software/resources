---
title: Snippets
description: You may find some of our code snippets useful for your own software journey!
category: Overview
position: 7
---

<cta-button text="Snippets Repo" link="https://gitlab.com/grey-software/snippets"></cta-button>

### How to write a package.json script that references another internal script & works with yarn or npm

When writing a script in your `package.json` that references another internal script, it is important to ensure that your script is compatible with both npm or yarn.

**Consider the following example**

```
{
    "name": "vue-content-scripts",
    "version": "1.2.1",
    "scripts": {
        "build": "vite build",
        "build:watch": "yarn build -- --watch"
    },
    "dependencies": {
        "vite": "^2.3.7"
    }
}
```

**💡 In case one of your team members cannot run either one of yarn or npm**, you can use `$npm_execpath` instead of `yarn` or `npm`.

```
{
    "name": "example-package",
    "version": "1.2.1",
    "scripts": {
        "build": "vite build",
        "build:watch": "$npm_execpath build -- --watch"
    },
    "dependencies": {
        "vite": "^2.3.7"
    }
}
```

#### 📚 Sources

- https://stackoverflow.com/questions/41647961/package-json-scripts-that-work-with-npm-and-yarn/46660467

### Organize files in a folder by file type in Bash

This bash script organizes files in a folder by file type.

#### Usage

```bash
bash organize-files.sh ~/
bash organize-files.sh ~/Desktop
bash organize-files.sh ~/Downloads
```

**organize-files.sh**

```bash
#!/bin/bash

# Usage Examples
# bash organize-files.sh ~/
# bash organize-files.sh ~/Desktop
# bash organize-files.sh ~/Downloads

folderPath=$1
echo "Organizing $folderPath"
organizedFolders=(Audio Video Documents Presentations Scripts Images Compressed Applications WebDev Data)

cd $folderPath

for folder in "${organizedFolders[@]}"
do
    echo "Creating $folderPath/$folder"
    mkdir $folderPath/$folder
done

# Audio Files
mv *.mp3 *.m4a *.flac *.aac *.ogg *.wav Audio

# Video Files
mv *.mp4 *.mov *.avi *.mpg *.mpeg *.webm *.mpv *.mp2 *.wmv Video

# Documents
mv *.pdf *.PDF *.doc *.docx *.txt *.odt Documents

# Presentations
mv *.ppt *.pptx Presentations

# Scripts
mv *.py *.rb *.sh Scripts

# Images
mv *.svg *.png *.jpg *.jpeg *.tif *.tiff *.bpm *.gif *.eps *.raw Images

# Compressed
mv *.tar.* *.xz *.txz *.tgz *.zip *.rar Compressed

# Applications
mv *.exe *.dmg Applications

# WebDev
mv *.js *.css *.html *.ts WebDev

# Data
mv *.yaml *.json *.yml *.csv Data

echo "Organized ✅"
```
