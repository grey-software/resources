---
title: Backend Development 
description: We're compiling a library of useful roadmaps in the domain of Backend Development from around the Internet to help you on your software journey!
position: 201
category: Roadmaps
---

## 2022

### Backend Roadmap

Step by step guide to become a modern backend developer.

[![Backend Roadmap](https://i.imgur.com/PsGEnPQ.jpg)](https://roadmap.sh/backend)

[Source : Kamran Ahmed - GitHub](https://github.com/kamranahmedse)

<cta-button text="View Roadmap" link="https://roadmap.sh/backend"></cta-button>


### ASP.NET Core Developer Roadmap

Step by step guide to becoming an ASP.NET core developer.

[![ASP.NET Core Developer Roadmap 2022](https://i.imgur.com/CxHCkYx.png)](https://roadmap.sh/aspnet-core)

[Source : Kamran Ahmed - GitHub](https://github.com/kamranahmedse)

<cta-button text="View Roadmap" link="https://roadmap.sh/aspnet-core"></cta-button>


### Python Developer

Step by step guide to becoming a Python developer.

[![ASP.NET Core Developer Roadmap 2022](https://i.imgur.com/AAuKnLN.png)](https://roadmap.sh/python)

[Source : Kamran Ahmed - GitHub](https://github.com/kamranahmedse)

<cta-button text="View Roadmap" link="https://roadmap.sh/python"></cta-button>


## 2020

### Laravel Developer Roadmap 

[![Laravel Developer Roadmap 2020 Preview](https://i.imgur.com/MAE21W7.png)](https://github.com/Hasnayeen/laravel-developer-roadmap)

[Source : Nehal Hasnayeen - GitHub](https://github.com/Hasnayeen)

<cta-button text="View Roadmap" link="https://github.com/Hasnayeen/laravel-developer-roadmap"></cta-button>

