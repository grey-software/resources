---
title: Software Development
description: We're compiling a library of useful roadmaps in the domain of Software Development from around the Internet to help you on your software journey!
position: 205
category: Roadmaps
---

## 2022

### Computer Science Roadmap

Computer Science curriculum with free resources for a self-taught developer.

[![Computer Science 2022](https://i.imgur.com/9bltQTf.png)](https://roadmap.sh/computer-science)

[Source : Kamran Ahmed - GitHub](https://github.com/kamranahmedse)

<cta-button text="View Roadmap" link="https://roadmap.sh/computer-science"></cta-button>



### DevOps Roadmap

Step by step guide for DevOps, SRE or any other Operations Role.

[![DevOps Roadmap 2022](https://i.imgur.com/eNMgRHK.png)](https://roadmap.sh/devops)

[Source : Kamran Ahmed - GitHub](https://github.com/kamranahmedse)

<cta-button text="View Roadmap" link="https://roadmap.sh/devops"></cta-button>


### Software Architect

Step by step guide to becoming a Software Architect.

[![Software Architect Roadmap 2022](https://i.imgur.com/RRPAhIo.png)](https://roadmap.sh/software-architect)

[Source : Kamran Ahmed - GitHub](https://github.com/kamranahmedse)

<cta-button text="View Roadmap" link="https://roadmap.sh/software-architect"></cta-button>


### Software Design and Architecture

Step by step guide to learn software design and architecture.

[![Software Design and Architecture Roadmap 2022](https://i.imgur.com/H0ADRZ0.png)](https://roadmap.sh/software-design-architecture)

[Source : Kamran Ahmed - GitHub](https://github.com/kamranahmedse)

<cta-button text="View Roadmap" link="https://roadmap.sh/software-design-architecture"></cta-button>

