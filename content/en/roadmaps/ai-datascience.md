---
title: AI & Data Science
description: We're compiling a library of useful roadmaps in the domain of AI / Machine Learning and Data Science from around the Internet to help you on your software journey!
position: 203
category: Roadmaps
---

## 2022

### Artificial Intelligence Expert Roadmap

[![Data Engineer Roadmap](https://i.imgur.com/EKhfogf.png)](https://github.com/AMAI-GmbH/AI-Expert-Roadmap)

[Source : AMAI GmbH - GitHub](https://github.com/AMAI-GmbH)

<cta-button text="View Roadmap" link="https://github.com/AMAI-GmbH/AI-Expert-Roadmap"></cta-button>


## 2021

### Data Engineer Roadmap

[![Data Engineer Roadmap](https://i.imgur.com/OdyN6DF.jpg)](https://github.com/datastacktv/data-engineer-roadmap)

[Source : datastack.tv - GitHub](https://github.com/datastacktv)

<cta-button text="View Roadmap" link="https://github.com/datastacktv/data-engineer-roadmap"></cta-button>
