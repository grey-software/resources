---
title: Books
description: We're compiling a library of useful Quantum Computing books from around the Internet to help you on your software journey!
position: 15000
category: Quantum Computing
---

## Quantum Computing

### Introduction to Classical and Quantum Computing 

<book-cover link="http://www.thomaswong.net/introduction-to-classical-and-quantum-computing-1e2p.pdf" img-src="https://i.imgur.com/s3LksIj.jpg" alt="Book cover for Introduction to Classical and Quantum Computing "> </book-cover>

*Introduction to Classical and Quantum Computing* is for newcomers who are interested in quantum computing as a potential career, but who may not be ready for advanced books or courses. The only prerequisite for this book is trigonometry, also called pre-calculus. You are not expected to have taken advanced math beyond that, and you are not expected to have experience with programming. So, if you are an advanced high school student or a beginning university student, this textbook is for you.

<cta-button text="Complete Book" link="http://www.thomaswong.net/introduction-to-classical-and-quantum-computing-1e2p.pdf"></cta-button>

### Learn Quantum Computation using Qiskit 

<book-cover link="https://qiskit.org/textbook/preface.html" img-src="https://i.imgur.com/uBoV2ry.png" alt="Book cover for Learn Quantum Computation using Qiskit "> </book-cover>

This is a free digital textbook that will teach you the concepts of quantum computing while you learn to use the Qiskit SDK

<cta-button text="Complete Book" link="https://qiskit.org/textbook/preface.html"></cta-button>

### Quantum Computing for the Quantum Curious

<book-cover link="https://link.springer.com/book/10.1007/978-3-030-61601-4" img-src="https://i.imgur.com/wcTtfUo.jpg" alt="Book cover for Quantum Computing for the Quantum Curious"> </book-cover>

This open access book makes quantum computing more accessible than ever before. A fast-growing field at the intersection of physics and computer science, quantum computing promises to have revolutionary capabilities far surpassing “classical” computation. Getting a grip on the science behind the hype can be tough: at its heart lies quantum mechanics, whose enigmatic concepts can be imposing for the novice. 

This classroom-tested textbook uses simple language, minimal math, and plenty of examples to explain the three key principles behind quantum computers: superposition, quantum measurement, and entanglement. It then goes on to explain how this quantum world opens up a whole new paradigm of computing. 

<cta-button text="Complete Book" link="https://link.springer.com/book/10.1007/978-3-030-61601-4"></cta-button>

</br>

<cta-button text="More Books On Quantum Computing" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-subjects.md#quantum-computing"></cta-button>