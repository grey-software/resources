---
title: Books
description: We're compiling a library of useful python books from around the Internet to help you on your software journey!
position: 20502
category: Python 
---


### A Practical Introduction to Python Programming 

<book-cover link="https://www.brianheinold.net/python/A_Practical_Introduction_to_Python_Programming_Heinold.pdf" img-src="https://i.imgur.com/grYl3mz.png" alt="Book cover for A Practical Introduction to Python Programming "> </book-cover>

This book is specifically designed to be used in an introductory programming course, but it is also useful for those with prior programming experience looking to learn Python.

<cta-button text="Download PDF" link="https://www.brianheinold.net/python/A_Practical_Introduction_to_Python_Programming_Heinold.pdf"></cta-button>


### Full Stack Python

<book-cover link="https://www.fullstackpython.com/" img-src="https://i.imgur.com/2sItjcV.jpg" alt="Book cover for Full Stack Python"> </book-cover>

Full Stack Python explains important Python concepts in plain language terms without assuming that you already know much about web development, deployments or running an application. Each chapter focuses on a large subject area, such as data or development environments, then digs into concepts and implementations that you should know to make the right choices for what to use when building your projects.

Think of Full Stack Python as your high-level guide to Python development so that you can understand what you do not know and gain a map for what to research next.

<cta-button text="Complete Book" link="https://www.fullstackpython.com/"></cta-button>


### Invent Your Own Computer Games With Python

<book-cover link="https://inventwithpython.com/invent4thed/" img-src="https://i.imgur.com/xQ2Wsea.png" alt="Book cover for Invent Your Own Computer Games With Python"> </book-cover>

Invent Your Own Computer Games with Python teaches you how to program in the Python language. Each chapter gives you the complete source code for a new game, and then teaches the programming concepts from the examples. Games include Guess the Number, Hangman, Tic Tac Toe, and Reversi. This book also has an introduction to making games with 2D graphics using the Pygame framework.

<cta-button text="Complete Book" link="https://inventwithpython.com/invent4thed/"></cta-button>

</br>

<cta-button text="More Books On Python" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#python"></cta-button>

<cta-button text="More Resources On Python" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#python"></cta-button>