---
title: Cheat Sheets
description: We're compiling a library of useful database books from around the Internet to help you on your software journey!
position: 5003
category: Databases
---


## SQL

### MySQL Cheatsheet

[![MySQL Cheatsheet Preview](https://i.imgur.com/ze2ICnl.png)](https://s3-us-west-2.amazonaws.com/dbshostedfiles/dbs/sql_cheat_sheet_mysql.pdf)

[Source: databasestar.com ](https://www.databasestar.com/)

<cta-button text="View" link="https://s3-us-west-2.amazonaws.com/dbshostedfiles/dbs/sql_cheat_sheet_mysql.pdf"></cta-button>


### PostgreSQL Cheatsheet 

[![PostgreSQL Cheatsheet Preview](https://i.imgur.com/GOJo3PT.png)](https://s3-us-west-2.amazonaws.com/dbshostedfiles/dbs/sql_cheat_sheet_pgsql.pdf)

[Source: databasestar.com ](https://www.databasestar.com/)

<cta-button text="View" link="https://s3-us-west-2.amazonaws.com/dbshostedfiles/dbs/sql_cheat_sheet_pgsql.pdf"></cta-button>


### Standard SQL Functions Cheat Sheet

The Standard SQL Functions Cheat Sheet provides you with the syntax for different text and numeric functions, CASE WHEN, NULLs, date and time types, INTERVALs, and aggregate functions. The Standard SQL Functions Cheat Sheet has a handy troubleshooting section and plenty of examples.

[![Standard SQL Functions Cheat Sheet Preview](https://i.imgur.com/wFtd7t9.png)](https://learnsql.com/blog/standard-sql-functions-cheat-sheet/)

[Source: learnsql.com](https://learnsql.com/)

<cta-button text="Download PDF" link="https://learnsql.com/blog/standard-sql-functions-cheat-sheet/"></cta-button>


### SQL Basics Cheat Sheet

[![SQL Cheat Sheet Preview](https://i.imgur.com/kN3t2U1.png)](https://learnsql.com/blog/sql-basics-cheat-sheet/sql-basics-cheat-sheet-a4.pdf)

[Source: learnsql.com](https://learnsql.com/)

<cta-button text="View" link="https://learnsql.com/blog/sql-basics-cheat-sheet/sql-basics-cheat-sheet-a4.pdf"></cta-button>


### SQL Commands Cheat sheet

[![SQL Commands Cheat sheet Preview](https://i.imgur.com/W0FRoEj.png)](https://intellipaat.com/mediaFiles/2019/02/SQL-Commands-Cheat-Sheet.pdf)

[Source: intellipaat.com](https://intellipaat.com/)

<cta-button text="View" link="https://intellipaat.com/mediaFiles/2019/02/SQL-Commands-Cheat-Sheet.pdf"></cta-button>


</br>

<cta-button text="More Cheatsheets On SQL" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#sql"></cta-button>


## MongoDB

### MongoDB Cheat Sheet

[![MongoDB Cheat Sheet Preview ](https://i.imgur.com/bPHDc37.png)](https://www.mongodb.com/developer/quickstart/cheat-sheet/)

[Source : mongodb.com](https://www.mongodb.com/)

<cta-button text="View" link="https://www.mongodb.com/developer/quickstart/cheat-sheet/"></cta-button>


### MongoDB Cheat Sheet

[![MongoDB Cheat Sheet Preview](https://i.imgur.com/fCOjAYb.png)](https://blog.codecentric.de/files/2012/12/MongoDB-CheatSheet-v1_0.pdf)

[Source : codecentric.de](https://blog.codecentric.de/en/2012/12/mongodb-tutorial/)

<cta-button text="View" link="https://blog.codecentric.de/files/2012/12/MongoDB-CheatSheet-v1_0.pdf"></cta-button>


### Quick Cheat Sheet for Mongo DB Shell commands 

[![Quick Cheat Sheet for Mongo DB Shell commands Preview](https://i.imgur.com/kW3vsvA.png)](https://gist.github.com/michaeltreat/d3bdc989b54cff969df86484e091fd0c)

[Source : Michael Treat - GitHub ](https://gist.github.com/michaeltreat)

<cta-button text="View" link="https://gist.github.com/michaeltreat/d3bdc989b54cff969df86484e091fd0c"></cta-button>

</br>

<cta-button text="More Cheatsheets On MongoDB" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#mongodb"></cta-button>