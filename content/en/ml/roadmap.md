---
title: Roadmaps
description: We're compiling a library of useful roadmaps from around the Internet to help you build technologies in machine learning!
position: 9004
category: Machine Learning
---

## The Ultimate FREE Machine Learning Study Plan

A complete study plan to become a Machine Learning Engineer with links to all FREE resources. If you finish the list you will be equipped with enough theoretical and practical experience to get started in the industry!

[![The Ultimate FREE Machine Learning Study Plan Preview](https://i.imgur.com/gGodgCB.png)](https://github.com/python-engineer/ml-study-plan)

[Author : Patrick Loeber - GitHub](https://github.com/python-engineer)

<cta-button text="View" link="https://github.com/python-engineer/ml-study-plan"></cta-button>

