---
title: Introduction
description: We're compiling useful machine learning resources around the internet to help you on your software journey!
position: 9000
category: Machine Learning
---

## Machine Learning

Machine Learning is the process of teaching a computer how perform a task with out explicitly programming it. The process feeds algorithms with large amounts of data to gradually improve predictive performance.

<youtube-video id="PeMlggyqz0Y"> </youtube-video>

