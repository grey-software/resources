---
title: Assests 
description: We're compiling a library of useful assests from around the Internet to help you in machine learning!
position: 9005
category: Machine Learning
---


## ML + Design

A collection of resources for intersection of design, user experience, machine learning and artificial intelligence.

[![ML + Design Preview](https://i.imgur.com/CZCQxlo.png)](https://machinelearning.design/)

[Source : machinelearning.design](https://machinelearning.design/)

<cta-button text="View" link="https://machinelearning.design/"></cta-button>


## Machine Learning Interview Questions

[![Machine Learning Interview Questions Preview](https://i.imgur.com/cUeeQDh.png)](https://github.com/andrewekhalel/MLQuestions)

[Author : Andrew Khalel - GitHub](https://github.com/andrewekhalel)

<cta-button text="View" link="https://github.com/andrewekhalel/MLQuestions"></cta-button>

