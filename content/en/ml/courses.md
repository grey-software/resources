---
title: Courses
description: We're compiling useful machine learning courses from top universities to help you on your software journey!
position: 9001
category: Machine Learning
---

## Introduction to Machine Learning (MIT)

This course introduces principles, algorithms, and applications of machine learning from the point of view of modeling and prediction. 

It includes formulation of learning problems and concepts of representation, over-fitting, and generalization. 

The concepts in this course are exercised in supervised learning and reinforcement learning, with applications to images and to temporal sequences.

<youtube-video id="0xaLT4Svzgo?list=PLxC_ffO4q_rW0bqQB80_vcQB09HOA3ClV"> </youtube-video>

<cta-button text="Full Playlist" link="https://www.youtube.com/watch?list=PLxC_ffO4q_rW0bqQB80_vcQB09HOA3ClV&v=0xaLT4Svzgo"></cta-button>

## Machine Learning (Stanford)

This course provides a broad introduction to machine learning and statistical pattern recognition. 

You'll learn about both supervised and unsupervised learning as well as learning theory, reinforcement learning and control. 

You'll also explore recent applications of machine learning and design and develop algorithms for machines.

<youtube-video id="jGwO_UgTS7I?list=PLoROMvodv4rMiGQp3WXShtMGgzqpfVfbU"> </youtube-video>

<cta-button text="Full Playlist" link="https://www.youtube.com/watch?list=PLoROMvodv4rMiGQp3WXShtMGgzqpfVfbU&v=jGwO_UgTS7I"></cta-button>

## Machine Learning for Healthcare (MIT)

This course introduces students to machine learning in healthcare, including the nature of clinical data and the use of machine learning for risk stratification, disease progression modeling, precision medicine, diagnosis, subtype discovery, and improving clinical workflows.

<youtube-video id="vof7x8r_ZUA?list=PLUl4u3cNGP60B0PQXVQyGNdCyCTDU1Q5j"> </youtube-video>

<cta-button text="Full Playlist" link="https://www.youtube.com/watch?list=PLUl4u3cNGP60B0PQXVQyGNdCyCTDU1Q5j&v=vof7x8r_ZUA"></cta-button>
