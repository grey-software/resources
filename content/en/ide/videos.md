---
title: Videos
description: We're compiling useful IDE resources around the internet to help you to help you on your software journey!
position: 8500
category: IDE
---

## Visual Studio Code Productivity Tips and Speed Hacks

In this video you will learn VS Code tips and tricks that will help you write code faster, try out awesome new features and extensions that turn your editor into a full-blown IDE

<youtube-video id="ifTF3ags0XI"></youtube-video>