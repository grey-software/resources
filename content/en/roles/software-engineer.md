---
title: Software Engineer
description: We're compiling useful resources around the internet to help you become a better software enginner!
position: 7501
category: Roles
---

## How to "Google It" like a Senior Software Engineer

It is said that a Software Engineer is just a “professional googler" 😆.

This video will help you learn how to use Google like a 10x developer to get the information you need faster.

<youtube-video id="cEBkvm0-rg0"></youtube-video>

## 12 Best Career Paths for Software Engineers 

<youtube-video id="6_1LtgvYo7o"></youtube-video>



