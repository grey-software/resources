---
title: Graphic Designer
description: We're compiling useful resources around the internet to help you become a better graphic designer!
position: 7502
category: Roles
---

## Self Taught Graphic Designer

If you want to become a graphic designer by teaching yourself how to design, while saving years of your time studying in a design school? Kayung Caleb Lai have put together 7 steps that will take one full year to equip yourself to become a professional designer.

<youtube-video id="Y0gRp1bo2RM"></youtube-video>



