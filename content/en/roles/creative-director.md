---
title: Creative Director
description: We're compiling useful resources around the internet to help you become a better creative director!
position: 7500
category: Roles
---

## What does a Creative Director Do?

Many graphic designers and creative professionals aspire to be a creative director one day. But what does a creative director actually do all day? You might be surprised that it is not just judging design work and standing around cooling cool.

There are many more skills and responsibilities that a creative director has in managing and running a design department. Want to get promoted? Listen carefully and start to focus on and develop the skills I discuss in this video. You will be on your way to being a creative director in no time!

<youtube-video id="Af7Tos7f5_o"></youtube-video>

## Teamwork, Collaboration, and Feedback - Matthew Encina

What does it take to be a good creative director? How do you manage a creative team, delegate tasks and get exactly what you expect? What questions should you ask so you have a clear understanding of the creative brief?

This video will help you learn the do's and dont's of good communication. Creative Director [Matthew Encina](https://www.matthewencina.com/) leads the conversation on giving and receiving creative direction.

<youtube-video id="3jD8i1No1L0"> </youtube-video>

## 9 Steps To Becoming A Creative Director - Adam Morgan

<youtube-video id="aHPsfiGsxF4"> </youtube-video>
