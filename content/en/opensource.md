---
title: Open Source
description: Important open-source resources for your software journey!
category: Overview
position: 9
---

## What is open source?

The term "open source" refers to something people can modify and share because its design is publicly accessible.

The term originated in the context of software development to designate a specific approach to creating computer programs. Open source projects, products, or initiatives embrace and celebrate principles of open exchange, collaborative participation, rapid prototyping, transparency, meritocracy, and community-oriented development.

[![opensource.com Preview](https://i.imgur.com/q2AKuLR.png)](https://opensource.com/)

[Source : opensource.com](https://opensource.com/)

<cta-button text="View" link="https://opensource.com/"></cta-button>


## Opensource.org

[![opensource.org Preview](https://i.imgur.com/Ay6qDMl.png)](https://opensource.org/)

[Source : opensource.org](https://opensource.org/)

<cta-button text="View" link="https://opensource.org/"></cta-button>


## Awesomeopensource.com

[![awesomeopensource.com Preview](https://i.imgur.com/L855x3R.png)](https://awesomeopensource.com/)

[Source : awesomeopensource.com](https://awesomeopensource.com/)

<cta-button text="View" link="https://awesomeopensource.com/"></cta-button>


## Theopensourceway.org

[![theopensourceway.org Preview](https://i.imgur.com/fDJScpY.png)](https://www.theopensourceway.org/)

[Source : theopensourceway.org](https://www.theopensourceway.org/)

<cta-button text="View" link="https://www.theopensourceway.org/"></cta-button>
