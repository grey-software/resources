---
title: Resume Builder
description: We're compiling a library of useful free open source resume builders from around the Internet to help you create resumes!
position: 19501
category: Employment 
---

## Reactive Resume

Reactive Resume is a free and open source resume builder that's built to make the mundane tasks of creating, updating and sharing your resume as easy as 1, 2, 3.

With this app, you can create multiple resumes without losing the integrity and privacy of your data.

[![Reactive Resume preview](https://i.imgur.com/D791O1K.png)](https://rxresu.me/)

[Source : rxresu.me](https://rxresu.me/)

<cta-button text="View" link="https://rxresu.me/"></cta-button>