---
title: Web inspiration 
description: We're compiling inspiring examples from around the internet to help you learn website design.
position: 1119
category: Design
---

![](https://i.imgur.com/8AEe2n8.jpg)

## Landing Page Inspirations 

Landingfolio features the best landing page designs, templates, component and more on the web. Get inspired by real landing page examples curated by their amazing team to ensure the highest quality.

[![Landing Page Inspirations Preview ](https://i.imgur.com/7hY72gf.png)](https://www.landingfolio.com/)

[Source : landingfolio.com](https://www.landingfolio.com/)

<cta-button text="Learn More" link="https://www.landingfolio.com/"></cta-button>


## Live Websites

This is an inspirational resource showcasing totally rocking websites made by people from all over the world.

[![Live Websites Preview](https://i.imgur.com/Zee60rZ.png)](https://httpster.net/2022/jul/)

[Source : httpster.net](https://httpster.net/2022/jul/)

<cta-button text="Learn More" link="https://httpster.net/2022/jul/"></cta-button>


## Free Personal Website Templates & Resources

Best 25 best HTML and WordPress personal website templates, mostly free, to show you how to create a personal website without any effort.

[![Free Personal Website Templates & Resources Preview](https://i.imgur.com/YOeaRGj.png)](https://blog.prototypr.io/25-best-free-personal-website-templates-and-resources-8c4101761ba)

[Author : Amy Smith - Medium](https://soulless.medium.com/)

<cta-button text="Learn More" link="https://blog.prototypr.io/25-best-free-personal-website-templates-and-resources-8c4101761ba"></cta-button>


## More Resources 

| Websites &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;| Description |
| ----------------------- | ----------------------------------|
| [onepagelove.com](https://onepagelove.com/)  | Get Inspiration, browse Templates, find Domains or learn from their Resources. |
| [httpster.net](https://httpster.net/2022/oct/) | An inspiration resource showcasing totally rocking websites made by people from all over the world. |
| [klart.io/pixels](https://klart.io/pixels) | Best known for kick-ass designs and copywriting, where new designs are being shared here everyday. |
| [siteinspire.com](https://www.siteinspire.com/) | A showcase of the finest web and interactive design. |
| [sitesee.co](https://sitesee.co/) | A curated gallery of beautiful, modern websites. |
| [siiimple.com](https://siiimple.com/) | A minimalist css gallery. Hand-picked, obsessively curated collection of the most beautiful websites on the internets. |
| [godly.website](https://godly.website/) | Discover astronomically good web design inspiration from all over the Internet.  |
| [awwwards.com](https://www.awwwards.com/) | Awards that recognize the talent and effort of the best web designers, developers and agencies in the world. |
| [landing.love](https://www.landing.love/) | Good web design inspiration from all over the Internet. |
| [bestwebsite.gallery](https://bestwebsite.gallery/) | The most beautiful websites handpicked for you — since 2008!  |
| [lapa.ninja](https://www.lapa.ninja/) | The Best Landing Page Examples Learn Design, Download Free Books & UI Kits. |
| [land-book.com](https://land-book.com/) | Find the best hand-picked website design inspiration. |


