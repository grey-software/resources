---
title: Design Exercises
description: We're compiling useful design exercises around the internet to help you on your design journey!
position: 1118
category: Design 
---

## Weekly Product Design Exercise

Receive weekly exercises inspired and used by companies like Facebook, Google, and WeWork to upgrade your UX design skills.

<cta-button text="Learn More" link="https://productdesigninterview.com/weekly-ux-product-design-exercise"></cta-button>
