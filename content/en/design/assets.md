---
title: Assets
description: We're compiling useful design assets around the internet to help you with your design skills.
position: 1110
category: Design
---

## Eva Design Assets

Eva Design is customizable design system with mobile and web component libraries.

Its design assets easily adaptable to your brand. Available for Sketch and Figma component libraries, based on Eva Design System.

[![Eva Design Assets Preview](https://i.imgur.com/kgwwRJj.png)](https://eva.design/)

[Source : eva.design](https://eva.design/)

<cta-button text="Learn More" link="https://eva.design/"></cta-button>


##  Open Source Design

This amazing resource is a showcase of the many websites and platforms where you can find openly licensed icons, fonts, image, tools and other resources.

You can use them for any purpose, also commercial (some works have specific licenses, so always make sure it’s fine to use).

[![Open Source Design Preview](https://i.imgur.com/WjDvORB.png)](https://opensourcedesign.net/resources/)

[Source : opensourcedesign.net](https://opensourcedesign.net/#page-top)

<cta-button text="Learn More" link="https://opensourcedesign.net/resources/"></cta-button>


## Design Articles 

A platform where you can find some insightful articles about design systems.

[![Design Articles Preview](https://i.imgur.com/hcvf7Bo.png)](https://designsystemsrepo.com/articles/)

[Source : designsystemsrepo.com](https://designsystemsrepo.com/)

<cta-button text="Learn More" link="https://designsystemsrepo.com/articles/"></cta-button>


## Design Systems Gallery

A comprehensive and curated list of design systems, style guides and pattern libraries that you can use for inspiration.

[![Design Systems Gallery Preview](https://i.imgur.com/6AKxcuh.png)](https://designsystemsrepo.com/design-systems/)

[Source : designsystemsrepo.com](https://designsystemsrepo.com/)

<cta-button text="Learn More" link="https://designsystemsrepo.com/design-systems/"></cta-button>


## Penpot

Penpot is the first Open Source design and prototyping platform meant for cross-domain teams. 

Non dependent on operating systems, Penpot is web based and works with open web standards (SVG). For all and empowered by the community.

[![Penpot Preview](https://i.imgur.com/HvdqFHr.png)](https://penpot.app/)

[Source : penpot.app](https://penpot.app/)

<cta-button text="Learn More" link="https://penpot.app/"></cta-button>


## Learn How To Become A Better Designer.

You can be a student in university, intern at a company or senior designer at a startup, these resources will help you to level up your work.

[![Learn How To Become A Better Designer Preview](https://i.imgur.com/C1K0IU5.png)](https://1984.design/)

[Source : 1984.design](https://1984.design/)

<cta-button text="Learn More" link="https://1984.design/"></cta-button>


## UX Designer Tools & Resources

This is the best collection of UX Designer tools and resources gathered from various conferences and workshops in Istanbul.

[![UX Designer Tools & Resources Preview](https://i.imgur.com/5vbeuWy.png)](https://medium.com/@UXAliveTurkey/the-best-ux-designer-tools-resources-collection-24bf115d17bc)

[Source : medium.com](https://medium.com/@UXAliveTurkey)

<cta-button text="Learn More" link="https://medium.com/@UXAliveTurkey/the-best-ux-designer-tools-resources-collection-24bf115d17bc"></cta-button>


## A Visual Guide To Best UX Books To Read

UX Design is about addressing issues and making people’s life simpler by using intuitive and effective design. This curated list of UX books will teach you the fundamentals in a practical and precise way.

It’s a combination of design, product, and business reads. Knowledge of HCI, usability, design principles, as well as knowing parts of psychology and how to design for people, is essential for any UX Position.

[![A Visual Guide To Best UX Books To Read Preview](https://i.imgur.com/lOm3Ilr.png)](https://medium.com/@linhduy/a-visual-guide-to-best-ux-books-to-read-6700342c3aef)

[Source: medium.com](https://medium.com/@linhduy)

<cta-button text="Learn More" link="https://medium.com/@linhduy/a-visual-guide-to-best-ux-books-to-read-6700342c3aef"></cta-button>


## UX Design Books: What UXers Must Read in 2020 ？

Good design is never easy. It doesn’t matter where you’re at in UX design - beginner, intermediate, or expert, you should keep learning and working hard to improve your design skills.

UX books, one of the most essential learning resources, are the best way to learn UX design basics and improve your design skills quickly and systematically.

[![UX Design Books: What UXers Must Read in 2020  Preview](https://i.imgur.com/3F1N43K.png)](https://www.mockplus.com/blog/post/ux-design-books)

[Source: mockplus.com](https://www.mockplus.com/?home=1)

<cta-button text="Learn More" link="https://www.mockplus.com/blog/post/ux-design-books"></cta-button>


## Design Resources - Github

A vast range of design resources on Github. 

[![Design Resources - Github Preview](https://i.imgur.com/5krhI3Y.png)](https://github.com/bradtraversy/design-resources-for-developers)

[Source: bradtraversy - Github](https://github.com/bradtraversy)

<cta-button text="Learn More" link="https://github.com/bradtraversy/design-resources-for-developers"></cta-button>


## Design Resources - Notion

List of free Product (& UI/UX) Design resources.

[![Design Resources - Notion Preview](https://i.imgur.com/FUaS5fe.png)](https://www.notion.so/552ed20b2c2948cc8b284e59230d8868?v=3705e6f9547b40bc9301d8f83403577e)

<cta-button text="Learn More" link="https://www.notion.so/552ed20b2c2948cc8b284e59230d8868?v=3705e6f9547b40bc9301d8f83403577e"></cta-button>


## Remote Design Resources from the Community

This is one of the great platforms for Figma Community coming together to share resources, learnings, and best practices as designers learn new ways of working together remotely.

[![Remote Design Resources from the Community Preview](https://i.imgur.com/9bVEMCS.png)](https://www.notion.so/figmacommunity/Remote-Design-Resources-from-the-Community-e2af55fa7ace484bbe66d98ba3fd2020)

<cta-button text="Learn More" link="https://www.notion.so/figmacommunity/Remote-Design-Resources-from-the-Community-e2af55fa7ace484bbe66d98ba3fd2020"></cta-button>


## Design Newsletter

HeyDesigner is the daily curated design knowledge for product people, UXers, PMs, and design engineers.

[![Design Newsletter Preview](https://i.imgur.com/pE39Y7E.png)](https://heydesigner.com/topics/)

[Source : heydesigner.com](https://heydesigner.com/)

<cta-button text="Learn More" link="https://heydesigner.com/topics/"></cta-button>


## Designmodo: Create Website and Email Newsletter Design

All-in-one solution to create website and newsletter design to impress and engage your customers.

- Drag & Drop Builder

- Custom Fonts

- HTML/CSS & Javascript

- Upload Images and Video

- Updated Design

- Animation Effects

[![ Designmodo: Create Website and Email Newsletter Design Preview](https://i.imgur.com/TGruoPM.png)](https://designmodo.com/)

[Source : designmodo.com](https://designmodo.com/)

<cta-button text="Learn More" link="https://designmodo.com/"></cta-button>


## UX Library

Your one-stop website for UX Articles, Books, Resources, & More

[![UX Library Preview](https://i.imgur.com/l0jAh9s.jpg)](https://www.uxlibrary.org/)

[Source : uxlibrary.org](https://www.uxlibrary.org/)

<cta-button text="Learn More" link="https://www.uxlibrary.org/"></cta-button>


## Toptal Design Blog

The Toptal Design Blog is a hub for advanced design studies by professional designers in the Toptal network on all facets of digital design, ranging from detailed design tutorials to in-depth coverage of new design trends, techniques, and technologies.

[![Toptal Design Blog Preview](https://i.imgur.com/zP2psaR.png)](https://www.toptal.com/designers/blog)

[Source : toptal.com/designers](https://www.toptal.com/designers)

<cta-button text="Learn More" link="https://www.toptal.com/designers/blog"></cta-button>



