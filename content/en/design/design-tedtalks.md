---
title: TedTalks For Designers
description: We're compiling useful design testalks around the internet to help you understand digital design.
position: 1125
category: Design
---

## The First Secret of Design is Noticing

As human beings, we get used to "the way things are" really fast. But for designers, the way things are is an opportunity ... Could things be better? How? In this funny, breezy talk, the man behind the iPod and the Nest thermostat shares some of his tips for noticing - and driving - change.

<youtube-video id="9uOMectkCCs"> </youtube-video>


## Got a Wicked Problem? First, Tell Me How You Make Toast

Making toast doesn’t sound very complicated - until someone asks you to draw the process, step by step. Tom Wujec loves asking people and teams to draw how they make toast, because the process reveals unexpected truths about how we can solve our biggest, most complicated problems at work. Learn how to run this exercise yourself, and hear Wujec’s surprising insights from watching thousands of people draw toast.

<youtube-video id="_vS_b7cJn2A"> </youtube-video>


## Happy Maps

Mapping apps help us find the fastest route to where we’re going. But what if we’d rather wander? Researcher Daniele Quercia demos “happy maps” that take into account not only the route you want to take, but how you want to feel along the way.

<youtube-video id="AJg9SXIcPiM"> </youtube-video>


## 3 Ways Good Design Makes You Happy

In this talk from 2003, design critic Don Norman turns his incisive eye toward beauty, fun, pleasure and emotion, as he looks at design that makes people happy. He names the three emotional cues that a well-designed product must hit to succeed.

<youtube-video id="RlQEoJaLQRA"> </youtube-video>


## Happiness By Design

Graphic designer Stefan Sagmeister takes the audience on a whimsical journey through moments of his life that made him happy and notes how many of these moments have to do with good design.

<youtube-video id="eZp-H9g_jeY"> </youtube-video>


## More Design TedTalks

| TedTalks&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; | 
| ----------------------- |
| [How Giant Websites Design for You (And a Billion Others, Too)](https://www.ted.com/talks/margaret_gould_stewart_how_giant_websites_design_for_you_and_a_billion_others_too) |
| [My Life in Typefaces](https://www.ted.com/talks/matthew_carter_my_life_in_typefaces) |
| [How I Learned to Stop Worrying and Love ‘Useless’ Art](https://www.ted.com/talks/luke_syson_how_i_learned_to_stop_worrying_and_love_useless_art) |
| [Design With The Blind in Mind](https://www.ted.com/talks/chris_downey_design_with_the_blind_in_mind) |
| [Design for All 5 Senses](https://www.ted.com/talks/jinsop_lee_design_for_all_5_senses) |
| [We Are All Designers ](https://www.ted.com/talks/john_hockenberry_we_are_all_designers)  |
| [Designing for Simplicity ](https://www.ted.com/talks/john_maeda_designing_for_simplicity) |
| [Great Design is Serious, Not Solemn ](https://www.ted.com/talks/paula_scher_great_design_is_serious_not_solemn) |
| [Design and Destiny ](https://www.ted.com/talks/philippe_starck_design_and_destiny) |
| [Can Design Save Newspapers? ](https://www.ted.com/talks/jacek_utko_can_design_save_newspapers)
| [What Your Designs Say About You ](https://www.ted.com/talks/sebastian_deterding_what_your_designs_say_about_you) |
| [How to Build Your Creative Confidence ](https://www.ted.com/talks/david_kelley_how_to_build_your_creative_confidence?language=en) |
| [Design Is In the Details](https://www.ted.com/talks/paul_bennett_design_is_in_the_details) |
| [3 Ways to (Usefully) Lose Control of Your Brand](https://www.ted.com/talks/tim_leberecht_3_ways_to_usefully_lose_control_of_your_brand) |
| [Design, Explained.](https://www.ted.com/talks/john_hodgman_design_explained) |


