---
title: Design Podcasts
description: We're compiling useful design podcasts around the internet to help you on your design journey!
position: 1117
category: Design 
---

## Sustain Open Source Design

S.O.S. Design is a podcast dedicated to exploring the intersection of open source and design: 

- How design is crucial in the open source ecosystem?
- How designers work with coders to make open source software better?
- What sustainability means for the field of open source designers? 

This podcast grew out of the Sustain community and Open Source Design, and seeks to share great conversations with members from both communities and the open source and design space at large.


<cta-button text="Listen & Subscribe" link="https://sosdesign.sustainoss.org/"></cta-button>

## The Futur Podcast

A weekly show that explores the interesting overlap between design, marketing, and business. Listen to host Chris Do hold candid conversations with inspirational people from the worlds of design, technology, marketing, business, philosophy and personal development.

<cta-button text="Listen & Subscribe" link="https://thefutur.com/podcast"></cta-button>

