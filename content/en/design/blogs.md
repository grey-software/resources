---
title: Blogs
description: We're compiling useful resources around the internet to help you learn UX design.
position: 1112
category: Design
---

## How To Start In UX

A free, self-guided class to help you take your first steps into digital product design.

[![How to start in UX Preview](https://i.imgur.com/uGPwWMT.png)](https://start.uxdesign.cc/)

[Source : start.uxdesign](https://start.uxdesign.cc/)

<cta-button text="Learn More" link="https://start.uxdesign.cc/"></cta-button>


## You Don’t Need To Know Everything About UX

This is a story about anxiety, where the author [Fabricio Teixeira](https://twitter.com/fabriciot) counsels the UX designers that they don’t need to be a specialist in all possible verticals within User Experience Design immediately as it is process of continues learning and growth.

[![You don’t need to know everything about UX Preview](https://i.imgur.com/5aJ5O86.jpg)](https://uxdesign.cc/journey/home)

[Author : Fabricio Teixeira - Medium.com](https://fabriciot.medium.com/)

<cta-button text="Learn More" link="https://uxdesign.cc/journey/home"></cta-button>


## What They Don’t Tell You About The UX Industry Starting Out

This article has a lot of potential advices regarding User Experience industry and  will help you get a firm grasp of what UX is.

[![What They Don’t Tell You About The UX Industry Starting Out Preview](https://i.imgur.com/EM5EYSR.png)](https://bootcamp.uxdesign.cc/what-they-dont-tell-you-about-the-ux-industry-starting-out-7f2dfc1617cf)

[Author : Anik Ahmed - Medium.com](https://medium.com/@AnikAhmedExperience)

<cta-button text="Learn More" link="https://bootcamp.uxdesign.cc/what-they-dont-tell-you-about-the-ux-industry-starting-out-7f2dfc1617cf"></cta-button>


## UX vs UI vs IA vs IxD : 4 Confusing Digital Design Terms Defined

This blog explains the difference between four digital design terms.

[![UX vs UI vs IA vs IxD : 4 Confusing Digital Design Terms Defined Preview](https://i.imgur.com/uAbXCSY.png)](https://uxplanet.org/ux-vs-ui-vs-ia-vs-ixd-4-confusing-digital-design-terms-defined-1ae2f82418c7)

[Author : Vincent Xia - Medium.com](https://medium.com/@Vincentxia77)

<cta-button text="Learn More" link="https://uxplanet.org/ux-vs-ui-vs-ia-vs-ixd-4-confusing-digital-design-terms-defined-1ae2f82418c7"></cta-button>


## User Experience For Product Designers

This blog explains five simple steps product designers can use to build better web apps.

[![User Experience for Product Designers Preview](https://i.imgur.com/wv0kZzO.png)](https://medium.com/looks-good-feels-good/user-experience-for-product-designers-e9fa621ce3bc)

[Author : Paul Hershey - Medium.com](https://medium.com/@paulhershey)

<cta-button text="Learn More" link="https://medium.com/looks-good-feels-good/user-experience-for-product-designers-e9fa621ce3bc"></cta-button>


## How To Get A Job As UX Designer

This blog explains 5 simple steps that you can follow to land a job as UX Designer with no experience.

[![How to get a Job as UX Designer Preview](https://i.imgur.com/fvQfaLq.png)](https://soulless.medium.com/5-simple-steps-to-land-a-job-as-ux-designer-with-no-experience-ac7729ca0c7a)

[Author : Amy Smith - Medium.com](https://soulless.medium.com/)

<cta-button text="Learn More" link="https://soulless.medium.com/5-simple-steps-to-land-a-job-as-ux-designer-with-no-experience-ac7729ca0c7a"></cta-button>


## How to Become a UX / UI Designer, with no Work Experience and Degree

This blog gives a detailed overview of 9 steps that you can follow to become a UX / UI designer, if you do not have a work experience and a degree.

[![How to Become a UX / UI Designer, with no Work Experience and Degree Preview](https://i.imgur.com/nbNDemM.png)](https://blog.prototypr.io/9-steps-how-to-become-a-ux-ui-designer-if-you-do-not-have-a-work-experience-and-a-degree-15c488824c81)

[Author : Sati Taschiba - Medium.com](https://medium.com/@satitaschiba)

<cta-button text="Learn More" link="https://blog.prototypr.io/9-steps-how-to-become-a-ux-ui-designer-if-you-do-not-have-a-work-experience-and-a-degree-15c488824c81"></cta-button>


## 4 Steps For Choosing The Right Projects For Your UX Portfolio

This article will give you a framework for deciding which projects make sense for you and your career goals.

[![4 steps for choosing the right projects for your UX portfolio Preview](https://i.imgur.com/psGp8X3.jpg)](https://www.invisionapp.com/inside-design/4-steps-for-choosing-projects/)

[Author : Sarah Doody - Invisionapp.com](https://www.invisionapp.com/inside-design/author/sarah-doody-ux-designer/)

<cta-button text="Learn More" link="https://www.invisionapp.com/inside-design/4-steps-for-choosing-projects/"></cta-button>


## How To Craft An Outstanding Case Study for Your UX Portfolio

UX case study relies on excellent storytelling with a clear, understandable structure.

This article breaks down the anatomy of a UX case study to help you tell a simple and effective story that shows off your skills.

[![How To Craft An Outstanding Case Study for Your UX Portfolio Preview](https://i.imgur.com/ZO2f9Um.png)](https://careerfoundry.com/en/blog/ux-design/ux-case-study/)

[Author : Jonny Grass](https://www.jonnygrass.com/)

<cta-button text="Learn More" link="https://careerfoundry.com/en/blog/ux-design/ux-case-study/"></cta-button>


## The Case Study Factory

Case Study Factory is a platform which facilitates many students and designers who are looking for an opportunity to have their work reach a broader audience. 

[![The Case Study Factory Preview](https://i.imgur.com/PXA60Jn.png)](https://essays.uxdesign.cc/case-study-factory/)

[Source : essays.uxdesign.cc](https://essays.uxdesign.cc/)

<cta-button text="Learn More" link="https://essays.uxdesign.cc/case-study-factory/"></cta-button>







