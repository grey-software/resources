---
title: Figma
description: We're compiling useful resources around the internet to help you learn figma.
position: 1105
category: Design
---

## What's Figma?

Figma is where teams design together. Bring ideas to life in a design, wireframe, or prototype. Partner with teammates from content creation to design implementation. Get better feedback from your stakeholders. Figma gets everyone on the same page—literally.

<youtube-video id="Cx2dkpBxst8"> </youtube-video>


## Beginners Guide

If you’re brand new to Figma or design tools, check out this playlist for a quick tour of key features as Figma takes you through a rough approximation of the design process.

<youtube-video id="dXQ7IHkTiMM?list=PLXDU_eVOJTx7QHLShNqIXL1Cgbxj7HlN4"> </youtube-video>

<cta-button text="Full Playlist" link="https://www.youtube.com/playlist?list=PLXDU_eVOJTx7QHLShNqIXL1Cgbxj7HlN4"></cta-button>

<cta-button text="Short Guide" link="https://help.figma.com/hc/en-us/sections/4405269443991-Figma-for-Beginners-4-parts-"></cta-button>


## Our Figma File

[![Our Figma File Preview](https://i.imgur.com/fvIiWyu.png)](http://figma.grey.software)

<cta-button text="View" link="http://figma.grey.software"></cta-button>


## Community Files

Explore thousands of templates, widgets, and plugins by the Figma Community.

[![Figma Community Preview](https://i.imgur.com/L6ixQGl.png)](https://www.figma.com/community)

[Source : figma.com](https://www.figma.com/)

<cta-button text="Visit" link="https://www.figma.com/community"></cta-button>


## Explore Figma Tutorials

Dive into Figma features and learn how to speed up your design workflow.

<youtube-video id="gnscqweM_NU?list=PLXDU_eVOJTx6zk5MDarIs0asNoZqlRG23"> </youtube-video>

<cta-button text="Full Playlist" link="https://www.youtube.com/playlist?list=PLXDU_eVOJTx6zk5MDarIs0asNoZqlRG23"></cta-button>


## Tips Playlist

This playlist contains ultra-short videos hosted by Figma's designer advocates to cover the most popular and requested Figma tips. 

<youtube-video id="CGWbit0BDQM?list=PLXDU_eVOJTx53btRMBES-ASBBm03-bUCJ"> </youtube-video>

<cta-button text="Full Playlist" link="https://www.youtube.com/playlist?list=PLXDU_eVOJTx53btRMBES-ASBBm03-bUCJ"></cta-button>


## Free Figma Resources

[![Figma Resources Preview](https://i.imgur.com/svD2Rbj.png)](https://liferay.design/resources/figma/)

[Source : Liferay.Design](https://liferay.design/)

<cta-button text="Learn More" link="https://liferay.design/resources/figma/"></cta-button>


## Much Needed Figma Plugins

| Plugins &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |  Description |
| ------------------------- | ------------------------| 
| [Benzin ](https://www.figma.com/community/plugin/912436486778130560/BENZIN---remove-background-for-free) | Remove background for free|
| [3D Icons](https://www.figma.com/community/file/1030350068466019692) |  Open source 3D icon library |
| [CoolHue ](https://www.figma.com/community/plugin/807561639084281386/CoolHue---Gradient-Color-Palette) | Gradient Color Palette |
| [BrandFetch](https://www.figma.com/community/plugin/733590967040604714/Brandfetch) | Instant access to any company’s brand assets. Search from millions of logos, colors, fonts, and more |
| [Mockup](https://www.figma.com/community/plugin/817043359134136295/Mockup)  |  Always up-to-date and ever-growing libarry of mockups |



## 5 Must Have Figma Plugins in 2022

**Benzin**

Remove background from images automatically. Just by one click and few seconds. Save your time!

[![](https://i.imgur.com/EH6qRoT.png)](https://www.figma.com/community/plugin/912436486778130560/BENZIN---remove-background-for-free)

<cta-button text="Try on Figma" link="https://www.figma.com/community/plugin/912436486778130560/BENZIN---remove-background-for-free"></cta-button>


**3D Icons**

Use 1400+ 3dicons within Figma or Figjam files quickly. The icons can be searched and filtered by color and angle.

[![](https://i.imgur.com/tC0dO5s.png)]( https://www.figma.com/community/plugin/1107546399747513238/3dicons)

<cta-button text="Try on Figma" link=" https://www.figma.com/community/plugin/1107546399747513238/3dicons"></cta-button>



**CoolHue**

CoolHue is the coolest handpicked gradient palette with 60+ ready to use gradients for your next amazing thing.

[![](https://i.imgur.com/QyDhwYn.png)](https://www.figma.com/community/plugin/807561639084281386/CoolHue---Gradient-Color-Palette)

<cta-button text="Try on Figma" link="https://www.figma.com/community/plugin/807561639084281386/CoolHue---Gradient-Color-Palette"></cta-button>



**BrandFetch**

Instant access to any company’s brand assets. Search from millions of logos, colors, fonts, and more.

[![](https://i.imgur.com/gInQtnt.png)](https://www.figma.com/community/plugin/733590967040604714/Brandfetch)

<cta-button text="Try on Figma" link=" https://www.figma.com/community/plugin/733590967040604714/Brandfetch"></cta-button>



**Mockup**

Just select a device, choose a frame, and your design appear on the screen. Easiest mockup plugin

[![](https://i.imgur.com/X1NQ9fr.png)](https://www.figma.com/community/plugin/817043359134136295/Mockup)

<cta-button text="Try on Figma" link=" https://www.figma.com/community/plugin/817043359134136295/Mockup"></cta-button>




