---
title: Frameworks 
description: We're compiling a library of useful Project Management frameworks from around the Internet to help you on your software journey!
position: 4002
category: Product Management
---

## Kanban VS Scrum

In this video, you will learn about two popular product management frameworks "Kanban" and "Scrum", their pros and cons, and the flexibility they possess.

<youtube-video id="zSVB8kh9rqs"> </youtube-video>