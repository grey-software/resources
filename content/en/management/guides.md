---
title: Guides
description: We're compiling a library of useful Project Management guides from around the Internet to help you on your software journey!
position: 4001
category: Product Management
---

## Open Product Management 

A detailed guide to learn about product management.

[![Open Product Management Preview](https://i.imgur.com/N6Q49hC.png)](https://github.com/ProductHired/open-product-management)

[Source : ProductHired - Github](https://github.com/ProductHired)

<cta-button text="Guide" link="https://github.com/ProductHired/open-product-management"></cta-button>