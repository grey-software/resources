---
title: Videos
description: We're compiling useful data science videos around the internet to help you on your software journey!
position: 10502
category: Data Science
---


## Solving Real World Data Science Tasks With Python (movie dataset creation)

In this video, you will learn to scrape Wikipedia pages to create a dataset on Disney movies. 

The video is formatted with tasks for you to try to solve on your own throughout. For the best learning experience, at each task you should pause the video, try the task on your own, and then resume when you want to see how would 
Keith solve it.

[Source : Keith Galli](https://keithgalli.com/)

<youtube-video id="Ewgy-G9cmbg"></youtube-video>


## Data Analysis With Python Pandas 

In this series of videos, you will learn how analyze data with pandas using practical examples and warmUp exercises.

<cta-button text="Full Playlist" link="https://www.youtube.com/playlist?list=PL_1pt6K-CLoDMEbYy2PcZuITWEjqMfyoA"></cta-button>


## Solving Real-World Data Science Interview Questions

In this video, a series of Data Science Interview questions are being tackled on Stratascratch. Starting with easy problems using Python Pandas and then progressively getting into more difficult ones.

[Source : Keith Galli](https://keithgalli.com/)

<youtube-video id="cc0HOiKN_ac"></youtube-video>


## What Projects Should You Do As A Fresh Data Analyst Looking For Job Opportunities 

In this video, you will learn how to build a data portfolio if you're looking for an entry-level data analyst/ data science position. A portfolio seems to be a lot of work but always keep in mind that there are tons of resources online to help you get started and this video is perfect starting point.

[Source : Thu Vu](https://medium.com/@vuthihienthu.ueb)

<youtube-video id="XYKuslcJp7A"></youtube-video>




