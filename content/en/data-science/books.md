---
title: Books
description: We're compiling useful data science books around the internet to help you on your software journey!
position: 10501
category: Data Science
---

## Data Science

### R for Data Science 

<book-cover link="https://r4ds.had.co.nz/" img-src="https://i.imgur.com/EPGrncK.png" alt="Book cover for R for Data Science "> </book-cover>

 This book will teach you how to do data science with R: You will learn how to get your data into R, get it into the most useful structure, transform it, visualize it, and model it. In this book, you will find a practicum of skills for data science and learn how to use the grammar of graphics, literate programming, and reproducible research to save time.
 
 In addition to that, you will also learn how to manage cognitive resources to facilitate discoveries when wrangling, visualizing and exploring data.

<cta-button text="Complete Book" link="https://r4ds.had.co.nz/"></cta-button>

### Python Data Science handbook 

<book-cover link="https://jakevdp.github.io/PythonDataScienceHandbook/" img-src="https://i.imgur.com/3NtnVhl.png" alt="Book cover for Python Data Science handbook"> </book-cover>

For many researchers, Python is a first-class tool mainly because of its libraries for storing, manipulating, and gaining insight from data. Several resources exist for individual pieces of this data science stack, but only with the Python Data Science Handbook do you get them all—IPython, NumPy, Pandas, Matplotlib, Scikit-Learn, and other related tools.

Working scientists and data crunchers familiar with reading and writing Python code will find this comprehensive desk reference ideal for tackling day-to-day issues: manipulating, transforming, and cleaning data; visualizing different types of data; and using data to build statistical or machine learning models. Quite simply, this is the must-have reference for scientific computing in Python

<cta-button text="Complete Book" link="https://jakevdp.github.io/PythonDataScienceHandbook/"></cta-button>

</br>

<cta-button text="More Books On Data Science" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#data-science"></cta-button>

