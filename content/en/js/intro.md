---
title: Introduction
description: We're curating useful Javascript resources from around the Internet to help you on your software journey!
position: 3000
category: Javascript
---

## What is JavaScript?

This video introduces JavaScript, perhaps the most important technology of the modern web.

- What is JavaScript?
- What are JavaScript's key features?
- What's the difference between web APIs used by browsers vs NodeJS?

<youtube-video id="09XmbByy6Sk"> </youtube-video>


