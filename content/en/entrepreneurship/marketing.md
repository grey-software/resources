---
title: Marketing 
description: We're compiling a library of useful marketing resources from around the Internet to help you on your software journey!
position: 1004
category: Entrepreneurship
---



## Personalized Marketing

In this playlist, you'll learn personalized marketing strategies with examples to help you make your audience react with marketing personalization.

<youtube-video id="c9XD5M5Za4c?list=PLQ_PJHym_RIivorCFY1XbQYHWmzGq75Dl"> </youtube-video>

<cta-button  link="https://www.youtube.com/playlist?list=PLQ_PJHym_RIivorCFY1XbQYHWmzGq75Dl" text="Complete Playlist"></cta-button>


## 5-Step Marketing Strategy To Grow Your Business

The goal of this article is to simplify this huge topic into a digestible guide full of insights from top marketing experts.  

[![5-Step Marketing Strategy to Grow Your Business Preview ](https://i.imgur.com/8YtLMyh.png)](https://www.shopify.com/blog/marketing-strategy)

[Author : Alexa Collins - shopify](https://www.shopify.com/blog/search?link_search=true&q=Alexa+Collins)

<cta-button  link="https://www.shopify.com/blog/marketing-strategy" text="Read Article"></cta-button>


## Marketing Examples

A gallery of finest real world marketing examples from successful companies

[![Marketing Examples Preview ](https://i.imgur.com/0g4UWkm.png)](https://marketingexamples.com/)

[Source : marketingexamples.com](https://marketingexamples.com/)

<cta-button  link="https://marketingexamples.com/" text="Read Articles"></cta-button>


## How To Estimate Market Size? (TAM or Total Addressable Market)

This video will help you learn basic definition of TAM, what are the most common mistakes made while calculating and presenting this to investors, and how to estimate it properly?

<youtube-video id="M_RMTC2YmXY"> </youtube-video>


## 3 Simple Steps To Price Your Product

This guide covers everything you need to know about how to price a product, and also goes over important components of an effective pricing strategy and popular pricing models used in business today.

[![3 Simple steps to price your product Preview Preview ](https://i.imgur.com/mn5TFXA.png)](https://www.shopify.com/blog/how-to-price-your-product)

[Author :  Desirae Odjick - shopify](hhttps://www.shopify.com/blog/search?link_search=true&q=Desirae+Odjick)

<cta-button  link="https://www.shopify.com/blog/how-to-price-your-product" text="Read Article"></cta-button>


## More Resources 

|Websites |
|------------------|
| [Marketing Checklist for new Startups](https://github.com/draftdev/startup-marketing-checklist/blob/master/marketing-checklist.md) |
| [30 must have Marketing Resources](https://www.nutshell.com/blog/best-online-marketing-resources) |
| [What you don't know about Marketing](https://youtu.be/BPK_qzeH_yk) |
| [A curated list of resources related to internet marketing.](https://github.com/cameronroe/awesome-marketing) |
| [Awesome List of Landing Pages and Other Marketing Resources](https://github.com/eibrahim/landing-pages-resources) |
| [A curated collection of marketing articles & tools to grow your product.](https://github.com/goabstract/Marketing-for-Engineers) |
| [Free Business and Marketing Resources - Ebooks, guides, templates, and more to help you grow.](https://www.hubspot.com/resources) |
| [43 Best website to learn Marketing by Buffer](https://buffer.com/library/marketing-resources/) |
| [Marketing for Startups by Hubspot](https://blog.hubspot.com/marketing/startup-marketing) | 
| [How to track Campaign Analytics](https://www.shopify.com/blog/14759449-how-to-track-your-marketing-campaigns-in-google-analytics)|
| [Soft skills for Business Negotiations and Marketing Strategies](https://www.youtube.com/watch?v=l2Wb-qfc50U&list=PLPjSqITyvDeVBPdMfCPj9upWjqdIwn1Tx) |













