---
title: Courses
description: We're compiling useful game development courses from top universitiesto help you on create video games!
position: 10000
category: Video Games Development
---


## Creating Video Games (MIT)

Creating Video Games is a class that introduces students to the complexities of working in small, multidisciplinary teams to develop video games. 

Students will learn creative design and production methods, working together in small teams to design, develop, and thoroughly test their own original digital games. 

Design iteration across all aspects of video game development (game design, audio design, visual aesthetics, fiction and programming) will be stressed. Students will also be required to focus test their games, and will need to support and challenge their game design decisions with appropriate focus testing and data analysis.


<youtube-video id="pfDfriSjFbY?list=PLUl4u3cNGP61V4W6yRm1Am5zI94m33dXk"> </youtube-video>

<cta-button text="Full Playlist" link="https://www.youtube.com/watch?list=PLUl4u3cNGP61V4W6yRm1Am5zI94m33dXk&v=pfDfriSjFbY"></cta-button>

