---
title: Cheat Sheets
description: We're compiling a library of useful Algorithms and Data Structures cheat sheets from around the Internet to help you on your software journey!
position: 12003
category: Algorithms and Data Structures
---

## Data Structures

[![Data Structures Preview](https://i.imgur.com/CccMXH8.png)](https://sites.google.com/view/datascience-cheat-sheets?pli=1#h.21yvgedqjov8)

[Source: aqeelanwar.github.io](https://aqeelanwar.github.io//)

<cta-button text="View" link="https://sites.google.com/view/datascience-cheat-sheets?pli=1#h.21yvgedqjov8"></cta-button>