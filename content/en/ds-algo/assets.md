---
title: Assests 
description: We're compiling a library of useful Algorithms and Data Structures assests from around the Internet to help you on your software journey!
position: 12002
category: Algorithms and Data Structures
---

## Learn Data Structures & Algorithms for FREE

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Learn Data Structures &amp; Algorithms for FREE: 🧵</p>&mdash; Mohammed Junaid 🎯 (@mdjunaidap) <a href="https://twitter.com/mdjunaidap/status/1559424684600557568?ref_src=twsrc%5Etfw">August 16, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

<cta-button text="View" link="https://twitter.com/mdjunaidap/status/1559424684600557568?s=20&t=rIrAt0uIOivvd7Kxsei7hg"></cta-button>


## Algorithm Visualizer

Algorithm Visualizer is an interactive online platform that visualizes algorithms from code.

[![Algorithm Visualizer Preview](https://i.imgur.com/QO8MWAa.png)](https://algorithm-visualizer.org/)

[Source: algorithm-visualizer.org](https://algorithm-visualizer.org/)

<cta-button text="View" link="https://algorithm-visualizer.org/"></cta-button>