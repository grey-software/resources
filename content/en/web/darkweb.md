---
title: The Deep/Dark Web
description: We're compiling useful resources around the internet to help you understand the deep web and dark web.
position: 2501
category: The Web
---

## Web vs Internet

In this video Dr Max Wilsone explains the basic difference between The Internet and the World Wide Web.

<youtube-video id="oiR2mvep_nQ?list=PLzH6n4zXuckpPcCIJigThQgx5CB5gPiC6"> </youtube-video>

## Deep Web vs Dark Web

Deep and Dark web are often being conflated with one another. In this video Dr Max Wilson untangles the difference.

<youtube-video id="joxQ_XbsPVw?list=PLzH6n4zXuckpPcCIJigThQgx5CB5gPiC6"> </youtube-video>

## What is TOR and How it Works

The Tor browser is a tool that anyone can download for Linux, Mac, Windows and mobile devices. The Tor browser is primarily used to protect your identity online. When using it, everything you do goes through their network and is encrypted, keeping your online activity private

<youtube-video id="QRYzre4bf7I?list=PLzH6n4zXuckpPcCIJigThQgx5CB5gPiC6"> </youtube-video>

## TOR Hidden Services

In this video Dr Mike Pound explains how the Dark web allows users to hide services using TOR.

<youtube-video id="lVcbq_a5N9I?list=PLzH6n4zXuckpPcCIJigThQgx5CB5gPiC6"> </youtube-video>
