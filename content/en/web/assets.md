---
title: Assets 
description: We're compiling useful web development assets around the internet to help you understand web .
position: 2505
category: The Web
---

## Fundamental Web Skills

A visual overview of useful skills to learn as a web developer.

[![Fundamental Web Skills Preview](https://i.imgur.com/Xew5EBC.png)](https://andreasbm.github.io/web-skills/?compact)

[Author : Andreas Mehlsen - GitHub](https://github.com/andreasbm)

<cta-button text="View" link="https://andreasbm.github.io/web-skills/?compact"></cta-button>


## AI-powered Code Review Assistant

With this AI powered tool you can save costly developer time by automating pull request summaries. Open a pull request and get a summary of the changes in seconds. furthermore, you can instantly understand the implications of small pull requests and get a huge headstart on big ones.

[![AI-powered Code Review Assistant Preview](https://i.imgur.com/5UDFScF.png)](https://whatthediff.ai/)

[Source : beyondco.de](https://beyondco.de/)

<cta-button text="View" link="https://whatthediff.ai/"></cta-button>