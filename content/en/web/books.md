---
title: Books
description: We're compiling useful web development books around the internet to help you understand web .
position: 2504
category: The Web
---


## Web Performance

### Book Of Speed

<book-cover link="https://www.bookofspeed.com/" img-src="https://i.imgur.com/nOhb8Tc.png" alt="Book cover for  Book of Speed"> </book-cover>

This book gives you practical industry examples and studies which expose the effects of performance on the bottom line

[Author : Stoyan Stefanov - Github](https://github.com/stoyan)

<cta-button text="Complete Book" link="https://www.bookofspeed.com/"></cta-button>


### High Performance Browser Networking

<book-cover link="https://hpbn.co/" img-src="https://i.imgur.com/oaTagrg.png" alt="Book cover for High Performance Browser Networking"> </book-cover>

This book provides a hands-on overview of what every web developer needs to know about the various types of networks (WiFi, 3G/4G), transport protocols (UDP, TCP, and TLS), application protocols (HTTP/1.1, HTTP/2), and APIs available in the browser (XHR, WebSocket, WebRTC, and more) to deliver the best—fast, reliable, and resilient—user experience.

[Author : Ilya Grigorik](https://www.igvita.com/)

<cta-button text="Complete Book" link="https://hpbn.co/"></cta-button>


### Mature Optimization

<book-cover link="https://carlos.bueno.org/optimization/mature-optimization.pdf" img-src="https://i.imgur.com/hW8WlW8.png" alt="Book cover for Mature Optimization"> </book-cover>

The trickiest part of speeding up a program is not doing it, but deciding whether it’s worth doing at all. There are few clear principles, only rules of thumb

[Author : Carlos Bueno](https://carlos.bueno.org/)

<cta-button text="Complete Book" link="https://carlos.bueno.org/optimization/mature-optimization.pdf"></cta-button>
</br>

<cta-button text="More Books On Web Performance" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-subjects.md#web-performance"></cta-button>


## Web Services

### RESTful Web Services

<book-cover link="http://restfulwebapis.org/RESTful_Web_Services.pdf" img-src="https://i.imgur.com/j12lWqB.png" alt="Book cover for RESTful Web Services"> </book-cover>

This book puts the "Web" back into web services. It shows how you can connect to the programmable web with the technologies you already use every day. The key is REST, the architectural style that drives the Web.

[ Authors : Leonard Richardson](https://www.crummy.com/) [and Sam Ruby](https://www.oreilly.com/pub/au/242)

<cta-button text="Complete Book" link="http://restfulwebapis.org/RESTful_Web_Services.pdf"></cta-button>

</br>

<cta-button text="More Books On Web Services" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-subjects.md#web-services"></cta-button>


## Search Engines

### Search Engines: Information Retrieval In Practice 

<book-cover link="https://ciir.cs.umass.edu/irbook/" img-src="https://i.imgur.com/hDqt5nw.png" alt="Book cover for Search Engines: Information Retrieval in Practice"> </book-cover>

This book is ideal for introductory information retrieval courses at the undergraduate and graduate level in computer science, information science and computer engineering departments. It is also a valuable tool for search engine and information retrieval professionals.

[Authors : W. Bruce Croft](https://scholar.google.com/citations?user=ArV74ZMAAAAJ)[, Donald Metzler](https://scholar.google.com/citations?user=bmXpOd8AAAAJ)[, Trevor Strohman](https://www.researchgate.net/scientific-contributions/Trevor-Strohman-70173601) 

<cta-button text="Complete Book" link="https://ciir.cs.umass.edu/irbook/"></cta-button>

</br>

<cta-button text="More Books On Search Engines" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-subjects.md#search-engines"></cta-button>