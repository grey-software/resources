---
title: Introduction
description: We're compiling useful resources around the internet to help you understand the web.
position: 2500
category: The Web
---

## What is "The Web" and how does it work?

In this video Jeremie Patonnier offers an overview of what the web is and how it works. This video can be very useful If you're just getting started on the web.

<youtube-video id="O_GWbkXIqEY?list=PLnRGhgZaGeBuCVMW7eNFUQCR6au0jGmcK"> </youtube-video>

## What's HTML and how does it communicate with web?

In this video, you will learn about the very first language of the web: HTML.

- What is HTML
- How does it communicate with web browsers to display pages the way you want?

<youtube-video id="PORRrz3Y8Vc?list=PLnRGhgZaGeBuCVMW7eNFUQCR6au0jGmcK"> </youtube-video>
