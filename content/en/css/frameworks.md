---
title: Frameworks
description: We're curating videos that help you learn CSS!
position: 3502
category: CSS & HTML
---


## CSS Frameworks

![](https://i.imgur.com/QLHhmXu.jpg)


| Websites &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;|
| ----------------------- | 
| [tailwindcss](https://tailwindcss.com/) |
| [Bootstrap](https://getbootstrap.com/)  |
| [Bulma](https://bulma.io/)  |
| [Zurb Foundation](https://get.foundation/) |
| [Materialize](https://materializecss.com/) |
| [Bootswatch](https://bootswatch.com/) | 
| [Tailwind components](https://tailwindcomponents.com/) |
| [Ready-to-use Tailwind CSS blocks](https://tailblocks.cc/)  |
| [tailwindUI - Tailwind UI Components](https://tailwindui.com/components)  |
| [Tailwind Cheat-Sheet](https://nerdcave.com/tailwind-cheat-sheet) |


## CSS Tools

![](https://i.imgur.com/CAKQnB3.jpg)

| Websites &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;|
| ----------------------- | 
| [flexbox](https://flexbox.io/) |
| [CSS GRID](https://cssgrid.io/) |
| [cssmastery](https://estelle.github.io/cssmastery/#slide1) |
| [CSS Button Generator](http://css3buttongenerator.com) |
| [Box Shadows](https://box-shadow.dev/) |
| [Flexplorer](https://bennettfeely.com/flexplorer/) |
| [CSS Grid Generator](https://cssgrid-generator.netlify.app/) |
| [CSS Specificity calculator](https://polypane.app/css-specificity-calculator/) |
| [Specificity calculator](https://specificity.keegan.st/) |
| [CSS clip-path Maker](https://bennettfeely.com/clippy/) |
| [Custom Shape Dividers](https://www.shapedivider.app/) |
| [Fancy-Border-Radius](https://9elements.github.io/fancy-border-radius/) |
| [Houdini.how](https://houdini.how/) |
| [Neumorphism.io](https://neumorphism.io/) |
| [Glassmorphism.com](https://glassmorphism.com/) |
| [3D Book](https://3dbook.xyz/) |
| [98.css](https://jdan.github.io/98.css/) |
| [Enjoy CSS - Online CSS Code Generator ](https://enjoycss.com/) |
| [CSS Arrow Please](https://cssarrowplease.com/) |
| [Image Effects with CSS](https://bennettfeely.com/image-effects/) |
