---
title: CSS Blogs
description: We're compiling CSS blogs that help you learn CSS!
position: 3504
category: CSS & HTML
---



## Frontend Blogs to follow

### CSS Tricks

[![CSS-Tricks Preview ](https://i.imgur.com/vIieReD.png)](https://css-tricks.com/)

[Source : css-tricks.com](https://css-tricks.com/)


### Smashing Magazine

[![Smashing Magazine Preview](https://i.imgur.com/hlbLtiY.png)](https://www.smashingmagazine.com/category/css/)

[Source : smashingmagazine.com](https://www.smashingmagazine.com)


### Code Pen

[![Code Pen Preview](https://i.imgur.com/H5KFHDC.png)](https://blog.codepen.io)

[Source : codepen.io](https://blog.codepen.io)


### Dev 

[![DEV.to Preview ](https://i.imgur.com/ZEpSnWk.png)](https://dev.to/t/css)

[Source : dev.to](https://dev.to/t/css)

