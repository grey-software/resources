---
title: Bids and Estimates
description: We're compiling a library of useful project management resources from around the Internet to help you on your software journey!
position: 19002
category: Project Management 
---


## How To Start & Run A Design Business: Pricing & Estimating Creative Design Jobs

This video will help you learn how to estimate a job based on time & materials. How to estimate a fixed bid and allow for profit margins and what do you need to know to start and run a design business? 

<youtube-video id="wd0ejVP_g78"> </youtube-video>


## How Do You Estimate The Cost Of A Project?

What should you add in your creative project bids to clients? Are you charging enough for your design work? How do you calculate overhead into your bids? What exactly can you charge for? How do you include profit into your bid?

This is an example of a bid for a Visual Effects job. It's common that creatives always undervalue how much it costs to produce a job, and end up losing money. Make sure you're charing enough on your next project.


<youtube-video id="6BKWq9VJd8w"> </youtube-video>
