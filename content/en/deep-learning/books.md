---
title: Books
description: We're compiling a library of useful deep learning books from around the Internet to help you on your software journey!
position: 11502
category: Deep Learning
---

## Deep Learning

### Dive into Deep Learning

<book-cover link="http://d2l.ai/" img-src="https://i.imgur.com/p59sEoK.jpg" alt="Book cover for Dive into Deep Learning"> </book-cover>

This book attempts to make deep learning approachable, teaching you the concepts, the context, and the code.

<cta-button text="Complete Book" link="http://d2l.ai/"></cta-button>


### Deep Learning - An MIT Press book

<book-cover link="https://www.deeplearningbook.org/" img-src="https://i.imgur.com/LA29d4C.jpg" alt="Book cover for Deep Learning"> </book-cover>

The Deep Learning textbook is a resource intended to help students and practitioners enter the field of machine learning in general and deep learning in particular

<cta-button text="Complete Book" link="https://www.deeplearningbook.org/"></cta-button>


</br>

<cta-button text="More Books On Deep Learning" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-subjects.md#machine-learning"></cta-button>


## R

### Advanced R Programming 

<book-cover link="http://adv-r.had.co.nz/" img-src="https://i.imgur.com/6yAQ4xs.jpg" alt="Book cover for Advanced R Programming "> </book-cover>

This book is designed primarily for R users who want to improve their programming skills and understanding of the language. It should also be useful for programmers coming to R from other languages, as it explains some of R’s quirks and shows how some parts that seem horrible do have a positive side

<cta-button text="Complete Book" link="http://adv-r.had.co.nz/"></cta-button>


### Data Analysis and Prediction Algorithms with R

<book-cover link="https://rafalab.github.io/dsbook/index.html#preface" img-src="https://i.imgur.com/I143iCQ.png" alt="Book cover for Data Analysis and Prediction Algorithms with R"> </book-cover>

The demand for skilled data science practitioners in industry, academia, and government is rapidly growing. This book introduces concepts from probability, statistical inference, linear regression and machine learning and R programming skills. Throughout the book we demonstrate how these can help you tackle real-world data analysis challenges.

<cta-button text="Complete Book" link="https://rafalab.github.io/dsbook/index.html#preface"></cta-button>


### The R Inferno

<book-cover link="https://www.burns-stat.com/pages/Tutor/R_inferno.pdf" img-src="https://i.imgur.com/Cm8qPjE.jpg" alt="Book cover for The R Inferno"> </book-cover>

An essential guide to the trouble spots and oddities of R. In spite of the quirks exposed here, R is the best computing environment for most data analysis tasks.

R is free, open-source, and has thousands of contributed packages. 

<cta-button text="Download PDF" link="https://www.burns-stat.com/pages/Tutor/R_inferno.pdf"></cta-button>

</br>

<cta-button text="More Books On R Programming" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#r"></cta-button>

<cta-button text="More Resources On R Programming" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#r"></cta-button>

## Python

### A Practical Introduction to Python Programming 

<book-cover link="https://www.brianheinold.net/python/A_Practical_Introduction_to_Python_Programming_Heinold.pdf" img-src="https://i.imgur.com/grYl3mz.png" alt="Book cover for A Practical Introduction to Python Programming "> </book-cover>

This book is specifically designed to be used in an introductory programming course, but it is also useful for those with prior programming experience looking to learn Python.

<cta-button text="Download PDF" link="https://www.brianheinold.net/python/A_Practical_Introduction_to_Python_Programming_Heinold.pdf"></cta-button>


### Full Stack Python

<book-cover link="https://www.fullstackpython.com/" img-src="https://i.imgur.com/2sItjcV.jpg" alt="Book cover for Full Stack Python"> </book-cover>

Full Stack Python explains important Python concepts in plain language terms without assuming that you already know much about web development, deployments or running an application. Each chapter focuses on a large subject area, such as data or development environments, then digs into concepts and implementations that you should know to make the right choices for what to use when building your projects.

Think of Full Stack Python as your high-level guide to Python development so that you can understand what you do not know and gain a map for what to research next.

<cta-button text="Complete Book" link="https://www.fullstackpython.com/"></cta-button>


### Invent Your Own Computer Games With Python

<book-cover link="https://inventwithpython.com/invent4thed/" img-src="https://i.imgur.com/xQ2Wsea.png" alt="Book cover for Invent Your Own Computer Games With Python"> </book-cover>

Invent Your Own Computer Games with Python teaches you how to program in the Python language. Each chapter gives you the complete source code for a new game, and then teaches the programming concepts from the examples. Games include Guess the Number, Hangman, Tic Tac Toe, and Reversi. This book also has an introduction to making games with 2D graphics using the Pygame framework.

<cta-button text="Complete Book" link="https://inventwithpython.com/invent4thed/"></cta-button>

</br>

<cta-button text="More Books On Python" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#python"></cta-button>

<cta-button text="More Resources On Python" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#python"></cta-button>