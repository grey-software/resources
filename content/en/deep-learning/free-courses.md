---
title: Courses
description: We're compiling a library of useful deep learning courses from around the Internet to help you on your software journey!
position: 11500
category: Deep Learning
---

## Free Courses

### Practical Deep Learning for Coders

Deep learning is a computer technique to extract and transform data–-with use cases ranging from human speech recognition to animal imagery classification–-by using multiple layers of neural networks.

A lot of people assume that you need all kinds of hard-to-find stuff to get great results with deep learning, but as you'll see in this course, those people are wrong.

<cta-button text="Course Part 1" link="https://course.fast.ai/"></cta-button>

<cta-button text="Course Part 2" link="https://course19.fast.ai/part2"></cta-button>

## University Courses 

### Deep Learning (MIT)

MIT's introductory course on deep learning methods with applications to computer vision, natural language processing, biology, and more! Students will gain foundational knowledge of deep learning algorithms and get practical experience in building neural networks in TensorFlow. 

<youtube-video id="7sB052Pz0sQ?list=PLtBw6njQRU-rwp5__7C0oIVt26ZgjG9NI"> </youtube-video>

<cta-button text="Full Playlist" link="https://www.youtube.com/playlist?list=PLtBw6njQRU-rwp5__7C0oIVt26ZgjG9NI"></cta-button>

### Introduction to Neural Computation (MIT)

This course introduces quantitative approaches to understanding brain and cognitive functions. Topics include mathematical description of neurons, the response of neurons to sensory stimuli, simple neuronal networks, statistical inference and decision making. It also covers foundational quantitative tools of data analysis in neuroscience: correlation, convolution, spectral analysis, principal components analysis, and mathematical concepts including simple differential equations and linear algebra.

<youtube-video id="PnJEj6TokDA?list=PLUl4u3cNGP61I4aI5T6OaFfRK2gihjiMm"> </youtube-video>

<cta-button text="Full Playlist" link="https://www.youtube.com/playlist?list=PLUl4u3cNGP61I4aI5T6OaFfRK2gihjiMm"></cta-button>


### Convolutional Neural Networks for Visual Recognition (Stanford)

Computer Vision has become ubiquitous in our society, with applications in search, image understanding, apps, mapping, medicine, drones, and self-driving cars. Core to many of these applications are visual recognition tasks such as image classification, localization and detection. Recent developments in neural network (aka “deep learning”) approaches have greatly advanced the performance of these state-of-the-art visual recognition systems

<youtube-video id="vT1JzLTH4G4?list=PL3FW7Lu3i5JvHM8ljYj-zLfQRF3EO8sYv"> </youtube-video>

<cta-button text="Full Playlist" link="https://www.youtube.com/playlist?list=PL3FW7Lu3i5JvHM8ljYj-zLfQRF3EO8sYv"></cta-button>


### Natural Language Processing with Deep Learning (Stanford)

Investigate the fundamental concepts and ideas in natural language processing (NLP), and get up to speed with current research. Students will develop an in-depth understanding of both the algorithms available for processing linguistic information and the underlying computational properties of natural languages. The focus is on deep learning approaches: implementing, training, debugging, and extending neural network models for a variety of language understanding tasks.

The course progresses from word-level and syntactic processing to question answering and machine translation. For their final project students will apply a complex neural network model to a large-scale NLP problem.

<youtube-video id="rmVRLeJRkl4?list=PLoROMvodv4rOSH4v6133s9LFPRHjEmbmJ"> </youtube-video>

<cta-button text="Full Playlist" link="https://www.youtube.com/playlist?list=PLoROMvodv4rOSH4v6133s9LFPRHjEmbmJ"></cta-button>



