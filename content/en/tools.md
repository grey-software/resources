---
title: Tools We Use
description: An overview of the Tools we use at Grey Software
category: Overview
position: 4
---

> An overview of the tools we use at Grey Software

## Software

### Github

![Github Preview](/tech-stack/github-preview.png)

GitHub is home to the largest software development community on the planet with over 50M+ developers and 100M+ projects. Grey software uses GitHub to manage collaborative open source software development projects and find the organization through the Github Sponsors program.

<cta-button  link="https://github.com/join" text="Sign Up" > </cta-button>

### Gitlab

![ Preview](/tech-stack/gitlab-preview.png)

Organizations rely on GitLab’s source code management, CI/CD, security, and more to deliver software rapidly. We use Gitlab along with Github because both services offer unique features that our organization takes advantage of.

<cta-button  link="https://gitlab.com/users/sign_up" text="Sign Up" > </cta-button>

<alert>
Why do we use both Github and Gitlab?
We use Gitlab for our primary development operations because we found it more conducive to handling the complexity of software project management. We use GitHub because of its vast community, classroom platform, and sponsorships program.</alert>

<br/>

## Design

### Figma

![Figma Preview](/tech-stack/figma-preview.png)

Figma is a web-based graphic and user interface design app. It allows you to collaborate on design files with your team in real time, and fits very well in the toolkit of a frontend web developer.

At Grey Software, we use Figma for our UI design and prototyping work.

<cta-button  link="https://www.figma.com/education/" text="Claim Edu Plan!" > </cta-button>

<cta-button  link="http://figma.grey.software" text="Our Figma File" > </cta-button>

<br/>

## Operations

### Plausible Analytics

![Plausible Preview](/tech-stack/plausible-preview.png)

<cta-button  link="http://org.grey.software/analytics" text="Our Analytics" > </cta-button>

## Languages

### Markdown

Most of our web content at Grey Software, [including this page you're on](https://gitlab.com/grey-software/resources/-/blob/master/content/en/our-tools.md), is written in Markdown.

<cta-button link="https://learn.grey.software/markdown/intro" text="Markdown Intro" > </cta-button>

<cta-button link="https://learn.grey.software/markdown/guide" text="Learn Markdown" > </cta-button>

### Javascript

Javascript is the language of the web, and it's difficult not to fall in love with it once you learn its quirks!

At Grey Software, Javascript is our primary language for frontend and backend applications!

<cta-button link="/js/intro" text="Javascript Resources" > </cta-button>

### Typescript

TypeScript is a superset of JavaScript that adds optional static typing to the language, which helps us catch mistakes while writing our code.

<youtube-video id="zQnBQ4tB3ZA?list=PL0vfts4VzfNiI1BsIK5u7LpPaIDKMJIDN"> </youtube-video>

<br/>

## Frameworks

### Vue

![vue Preview](/tech-stack/vue-preview.png)

Vue is a powerful Javascript framework that integrates well with classic web development tools and is a joy to learn and work with.

<cta-button link="https://vuejs.org" text="Learn More" > </cta-button>

#### Overview

<youtube-video id="nhBVL41-_Cw?list=PL0vfts4VzfNiI1BsIK5u7LpPaIDKMJIDN"> </youtube-video>

#### Why Use Vue?

<youtube-video id="p1iLqZnZPdo"> </youtube-video>

### Nuxt

![Nuxt Preview](https://nuxtjs.org/preview.png)

<cta-button  link="https://nuxtjs.org/docs/get-started/installation" text="Learn" > </cta-button>

<cta-button  link="https://nuxtjs.org/examples/routing/hello-world" text="Examples" > </cta-button>

#### Why Use Nuxt?

<youtube-video id="7ITypVi-qRY"> </youtube-video>

### Quasar

![Quasar Preview](/tech-stack/quasar-preview.png)

We used Quasar to build [Material Math](https://material-math.grey.software)

<cta-button link="https://quasar.dev" text="Learn More" > </cta-button>

<br/>

## Utilities

### Brave Browser and Firefox

![Browsers Preview](/tech-stack/browsers-preview.png)

Most of our projects management and development happens on web browsers, and there are two major open source engines in the market right now: Chromium and Firefox Quantum.

Add Grey software, we regularly use both browser engines for diverse compatability, and our favorite browsers to work with are Mozilla Firefox and Brave.

<cta-button  link="https://brave.com/download/" text="Brave Browser" > </cta-button> <cta-button  link="https://www.mozilla.org/en-US/firefox/new/" text="Download Firefox" > </cta-button>

### VisBug

![VisBug Preview](/tech-stack/visbug-preview.png)

VisBug is an open-source web extension that allows users to interact with their webpage using powerful tools that can edit elements and inspect styles.

Our team at Grey Software highly recommends having this extension since it has allowed us to treat the webpage as an art-board that we can edit and inspect before heading back into the code editor.

<cta-button  link="https://addons.mozilla.org/en-US/firefox/addon/visbug/" text="Firefox Add-On" > </cta-button> <cta-button  link="https://chrome.google.com/webstore/detail/visbug/cdockenadnadldjbbgcallicgledbeoc?hl=en" text="Chrome Extension" > </cta-button>

### ImageOptim

![ImageOptim Preview](/tech-stack/imageoptim-preview.png)

ImageOptim allows us to save disk space & bandwidth by compressing images without losing too much quality.

<cta-button  link="https://imageoptim.com/mac" text="Website" > </cta-button>
