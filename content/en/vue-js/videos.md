---
title: Videos
description: We're compiling a library of useful vue js videos from around the Internet to help you on your software journey!
position: 30500
category: Vue Js
---

## Vue.js

Vue.js is an open-source Javascript framework used to develop interactive web interfaces.

The creator of Vue.js envisioned a library that brought the best of React and Angular together in a framework that could be progressively adopted.

This video wil help you learn the basics of Vue and build your first reactive UI component.

<youtube-video id="nhBVL41-_Cw?list=PL0vfts4VzfNiI1BsIK5u7LpPaIDKMJIDN"> </youtube-video>