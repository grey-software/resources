---
title: Books
description: We're compiling a library of useful Cloud Computing books from around the Internet to help you on your software journey!
position: 15500
category: Cloud Computing
---

## Cloud Computing

### AWS Well-Architected Framework

<book-cover link="https://docs.aws.amazon.com/wellarchitected/latest/framework/welcome.html" img-src="https://i.imgur.com/9lDaRxC.jpg" alt="Book cover for AWS Well-Architected Framework"> </book-cover>

The AWS Well-Architected Framework helps you understand the pros and cons of decisions you make while building systems on AWS. By using the Framework you will learn architectural best practices for designing and operating reliable, secure, efficient, and cost-effective systems in the cloud

<cta-button text="Complete Book" link="https://docs.aws.amazon.com/wellarchitected/latest/framework/welcome.html"></cta-button>


### Cloud Design Patterns

<book-cover link="https://docs.microsoft.com/en-us/azure/architecture/patterns/" img-src="https://i.imgur.com/Oed9NUP.jpg" alt="Book cover for Cloud Design Patterns"> </book-cover>

These design patterns are useful for building reliable, scalable, secure applications in the cloud.

Each pattern describes the problem that the pattern addresses, considerations for applying the pattern, and an example based on Microsoft Azure. Most of the patterns include code samples or snippets that show how to implement the pattern on Azure. However, most of the patterns are relevant to any distributed system, whether hosted on Azure or on other cloud platforms.

<cta-button text="Complete Book" link="https://docs.microsoft.com/en-us/azure/architecture/patterns/"></cta-button>


### How to optimize storage costs using Amazon S3 - AWS 

<book-cover link="https://d1.awsstatic.com/product-marketing/S3/Amazon_S3_eBook_Cost_Optimization.pdf" img-src="https://i.imgur.com/E8T7IDq.png" alt="Book cover for How to optimize storage costs using Amazon S3 - AWS "> </book-cover>

In this eBook you will learn how to leverage Amazon S3 and its various storage classes to start reducing your storage costs today.

<cta-button text="Download PDF" link="https://d1.awsstatic.com/product-marketing/S3/Amazon_S3_eBook_Cost_Optimization.pdf"></cta-button>

</br>

<cta-button text="More Books On Cloud Computing" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-subjects.md#cloud-computing"></cta-button>
