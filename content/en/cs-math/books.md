---
title: Books
description:  We're compiling a library of free books about Math for Computer Science from around the Internet to help you on your software journey!
position: 13502
category: Math for CS
---

## Mathematics for Computer Science 

### Discrete Structures for Computer Science: Counting, Recursion, and Probability 

<book-cover link="https://cglab.ca/~michiel/DiscreteStructures/DiscreteStructures.pdf" img-src="https://i.imgur.com/AyFg9ST.gif" alt="Book cover for Discrete Structures for Computer Science: Counting, Recursion, and Probability "> </book-cover>

This is book for an undergraduate course on Discrete Structures for Computer Science students. It covers mathematical reasoning, basic proof techniques, sets, functions, relations, basic graph theory, asymptotic notation, and countability.

<cta-button text="Download PDF" link="https://cglab.ca/~michiel/DiscreteStructures/DiscreteStructures.pdf"></cta-button>


### Mathematics for Computer Science 

<book-cover link="https://courses.csail.mit.edu/6.042/spring18/mcs.pdf" img-src="https://i.imgur.com/i5REIh2.jpg" alt="Book cover for Mathematics for Computer Science"> </book-cover>

This book covers elementary discrete mathematics for computer science and engineering. It emphasizes mathematical definitions and proofs as well as applicable methods.

<cta-button text="Download PDF" link="https://courses.csail.mit.edu/6.042/spring18/mcs.pdf"></cta-button>

</br>

<cta-button text="More Books On Mathematics For CS" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-subjects.md#mathematics-for-computer-science"></cta-button>