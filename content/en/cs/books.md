---
title: Books
description:  We're compiling a library of useful computer science notes and books from around the Internet to help you on your software journey!
position: 505
category: Computer Science 
---

## Computer Science

### CS Principles: Big Ideas in Programming

<book-cover link="https://www.openbookproject.net/books/StudentCSP/" img-src="https://i.imgur.com/uGGMydt.png" alt="Book cover for CS Principles: Big Ideas in Programming"> </book-cover>

This book helps students learn what programming is and how it works without making you write lots of code. It will use the programming language Python, which is widely used, from companies like Google, Instagram and Facebook to engineering and scientific institutions like NASA and CERN.

<cta-button text="Complete Book" link="https://www.openbookproject.net/books/StudentCSP/"></cta-button>

</br>

<cta-button text="More Books On Computer Science" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-subjects.md#computer-science"></cta-button>


## Theoretical Computer Science

### Structure and Interpretation of Computer Programs

<book-cover link="http://sarabander.github.io/sicp/html/index.xhtml" img-src="https://i.imgur.com/e9pPJ5o.jpg" alt="Book cover for Structure and Interpretation of Computer Programs"> </book-cover>

Structure and Interpretation of Computer Programs has had a dramatic impact on computer science curricula over the past decade. This long-awaited revision contains changes throughout the text.

There are new implementations of most of the major programming systems in the book, including the interpreters and compilers, and the authors have incorporated many small changes that reflect their experience teaching the course at MIT since the first edition was published.

<cta-button text="Complete Book" link="http://sarabander.github.io/sicp/html/index.xhtml"></cta-button>


### Think Complexity - (2nd Edition) 

<book-cover link="https://greenteapress.com/wp/think-complexity-2e/" img-src="https://i.imgur.com/nS7TsOx.jpg" alt="Book cover for Think Complexity"> </book-cover>

This book is primarily about complexity science, but studying complexity science gives you a chance to explore topics and ideas you might not encounter otherwise, practice programming in Python, and learn about data structures and algorithms.

<cta-button text="Downlaod PDF" link="https://greenteapress.com/wp/think-complexity-2e/"></cta-button>


### Models of Computation - John E. Savage

<book-cover link="http://cs.brown.edu/people/jsavage/book/" img-src="https://i.imgur.com/8vEEgSf.jpg" alt="Book cover for Models of Computation"> </book-cover>

In Models of Computation: Exploring the Power of Computing, John Savage re-examines theoretical computer science, offering a fresh approach that gives priority to resource tradeoffs and complexity classifications over the structure of machines and their relationships to languages.

This viewpoint reflects a pedagogy motivated by the growing importance of computational models that are more realistic than the abstract ones studied in the 1950s, '60s and early '70s.

<cta-button text="Complete Book" link="http://cs.brown.edu/people/jsavage/book/"></cta-button>

</br>

<cta-button text="More Books On Theoretical Computer Science" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-subjects.md#theoretical-computer-science"></cta-button>


## Professional Development

### Professional Software Development For Students

<book-cover link="https://mixmastamyk.bitbucket.io/pro_soft_dev/intro.html" img-src="https://i.imgur.com/4sqSSk2.jpg" alt="Book cover for Professional Software Development For Students"> </book-cover>

As a student of computer science and programming, you’ve learned a significant portion of what you need to know as a rookie professional. The most difficult parts perhaps, but far from the “whole enchilada.” This book is meant to fill in the gaps in knowledge needed for workaday software projects.

It aims to teach “in a nutshell” , the myriad subjects the pro-developer should know. It covers the material in a broad manner from theory to practice, attempting to provide a platform for further study and a “mental-map” to place subjects into context.

<cta-button text="Complete Book" link="https://mixmastamyk.bitbucket.io/pro_soft_dev/intro.html"></cta-button>

</br>

<cta-button text="More Books On Professional Development" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-subjects.md#professional-development"></cta-button>


## Information Retrieval

### Information Retrieval: A Survey 

<book-cover link="https://www.csee.umbc.edu/csee/research/cadip/readings/IR.report.120600.book.pdf" img-src="https://i.imgur.com/h9bOlIZ.jpg" alt="Book cover for Information Retrieval: A Survey"> </book-cover>

The need for effective methods of automated IR has grown in importance because of the tremendous explosion in the amount of unstructured data, both internal, corporate document collections, and the immense and growing number of document sources on the Internet. This report is a tutorial and survey of the state of the art, both research and commercial, in this
dynamic field.

<cta-button text="Complete Book" link="https://www.csee.umbc.edu/csee/research/cadip/readings/IR.report.120600.book.pdf"></cta-button>


### Introduction to Information Retrieval

<book-cover link="https://nlp.stanford.edu/IR-book/information-retrieval-book.html" img-src="https://i.imgur.com/FpVUYDB.jpg" alt="Book cover for Introduction to Information Retrieval "> </book-cover>

The book aims to provide a modern approach to information retrieval from a computer science perspective.

<cta-button text="Complete Book" link="https://nlp.stanford.edu/IR-book/information-retrieval-book.html"></cta-button>

</br>

<cta-button text="More Books On Information Retrieval" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-subjects.md#information-retrieval"></cta-button>






