---
title: Courses
description: We're compiling a library of useful computer vision courses from around the Internet to help you on your software journey!
position: 7001
category: Computer Vision
---


## Introduction to Convolutional Neural Networks for Visual Recognition (Stanford)

Computer Vision has become ubiquitous in our society, with applications in search, image understanding, apps, mapping, medicine, drones, and self-driving cars. 

Core to many of these applications are visual recognition tasks such as image classification, localization and detection. 

Recent developments in neural network (aka “deep learning”) approaches have greatly advanced the performance of these state-of-the-art visual recognition systems.

<youtube-video id="vT1JzLTH4G4?list=PL3FW7Lu3i5JvHM8ljYj-zLfQRF3EO8sYv"> </youtube-video>

<cta-button text="Full Playlist" link="https://www.youtube.com/playlist?list=PL3FW7Lu3i5JvHM8ljYj-zLfQRF3EO8sYv"></cta-button>
