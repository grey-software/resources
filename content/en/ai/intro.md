---
title: Introduction
description: We're compiling a library of useful Artificial Intelligence resources from around the Internet to help you on your software journey!
position: 5500
category: Artificial Intelligence
---

## Artificial Intelligence.

Artificial intelligence (A.I.) is probably assumed to be something difficult to understand and comprehend.
But ultimately, A.I is the ability for computers to do things that are considered attributes of intelligence: processing language, understanding pictures, detecting patterns, etc.

In this video, Mozilla researcher Becca Ricks helps explain AI, algorithms and machine learning models.

<youtube-video id="P-iiN0c2uic?list=PLnRGhgZaGeBu1d-f0ZfaDOWrPMyN3IemM"></youtube-video>

## What Does AI Know About Me?

Artificial intelligence knows way more about you than you may realize. In this video, Katarzyna Szymielewicz explains the three layers of your digital profile.

<youtube-video id="dJWGgxIKgZA?list=PLnRGhgZaGeBu1d-f0ZfaDOWrPMyN3IemM"></youtube-video>

## Responsible Artificial Intelligence

A detailed guide every student must read in order to understand the basics of AI.

<cta-button text="Learn More" link="https://opendialogueonai.com/wp-content/uploads/2020/07/ENG_Delib.pdf"></cta-button>
